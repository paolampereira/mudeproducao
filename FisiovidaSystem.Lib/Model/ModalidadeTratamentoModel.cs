﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Lib.Model
{
    public class ModalidadeTratamentoModel
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public int VagasPorTurma { get; set; }
        public int Duracao { get; set; }
        public bool Inativo { get; set; }
        public string FotoCapa { get; set; }
        public int Tipo { get; set; }
        public string Descricao { get; set; }
        public List<Imagem> Galeria  { get; set; }
        public List<Icone> Icones { get; set; }
        public List<Usuario> Professores { get; set; }
        public Configuracao Configuracao { get; set; }
    }
}
