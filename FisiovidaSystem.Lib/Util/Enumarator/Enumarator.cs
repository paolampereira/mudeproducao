﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Util.Enumarator
{

    public enum enumConfiguracaoGeral
    {
        sessionModelKey,
        itensPorPagina,
        logCategory,
        facebookAppID,
        facebookAppSecret,
        facebookAppPermissao,
        facebookOptinAppID,
        facebookOptinAppPermissao,
        httpsHabilitado,
        urlDominioReplace,
        emailHost,
        emailPort,
        emailSSL,
        emailUser,
        emailPass,
        emailDefaultSender,
        emailSecurityProtocol,
        antivirusPrograma,
        antivirusParametros,
        antivirusVerificacao,
        razorNamespaces,
        dataEvento,
        hostUrlWithoutRequest,
        appContextoWithoutRequest
    }

}
