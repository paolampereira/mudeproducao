﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Enumerator
{
    public enum enumPerfilNome
    {
        master = 'M',
        professor = 'P',
        aluno = 'A'
    }
    public enum enumDiaSemana
    {
        [Description("Segunda")]
        segunda = 1,
        [Description("Terça")]
        terca = 2,
        [Description("Quarta")]
        quarta = 3,
        [Description("Quinta")]
        quinta = 4,
        [Description("Sexta")]
        sexta = 5,
        [Description("Sábado")]
        sabado = 6,
        [Description("Domingo")]
        domingo = 0
    }
    public enum enumConfiguracaoLib
    {
        MidiasDiretorio,
        MidiasUrl,
        MidiasUrlCrop,
        ImgDiretorio,
        ImgUrl
    }
}
