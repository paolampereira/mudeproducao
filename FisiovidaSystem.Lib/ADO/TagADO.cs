﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class TagADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private List<Tag> tags;

        public void Cadastrar(Tag tag)
        {
            string SQL = @"INSERT INTO Tag
                            (Nome, Cor, Diferencial, Descricao)
                        VALUES (@Nome, @Cor, @Diferencial, @Descricao)";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, tag);
                con.Close();
            }
        }

        public void Alterar(Tag tag)
        {
            string SQL = @"UPDATE public.tag
                           SET nome = @Nome, cor = @Cor,
                                Diferencial = @Diferencial, Descricao = @Descricao
                         WHERE id = @ID";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, tag);
                con.Close();
            }
        }

        public Tag Carregar(int id)
        {
            string SQL = @"SELECT 
                            ID, Nome, Cor, Descricao
                            FROM Tag
                            WHERE id = @id";

            Tag tag = new Tag();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                tag = con.Query<Tag>(SQL, new
                {
                    id = id
                }).FirstOrDefault();

                con.Close();
            }
            return tag;
        }

        public void Excluir(int id)
        {
            string SQL = @" DELETE FROM TagNovidade WHERE TagID = @id; 
                            DELETE FROM Tag WHERE ID = @id; ";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
        }

        public List<Tag> Listar(int skip, int take, string palavraChave, bool diferencial)
        {
            string SQL = @"SELECT 
                            ID, Nome, Cor
                            FROM Tag
                            WHERE lower(Nome) like @palavraChave AND Diferencial = @dif
                            ORDER BY Nome
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Tag
                            WHERE lower(Nome) like @palavraChave AND Diferencial = @dif";
            tags = new List<Tag>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                tags = con.Query<Tag>(SQL, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take,
                    dif = diferencial
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take,
                    dif = diferencial
                }).FirstOrDefault();


                con.Close();
            }
            return tags;
        }

        public List<Tag> Listar()
        {
            string SQL = @"SELECT 
                            ID, Nome, Cor
                            FROM Tag
                            WHERE Diferencial = false
                            ORDER BY Nome";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Tag
                            WHERE Diferencial = false";

            tags = new List<Tag>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                tags = con.Query<Tag>(SQL).ToList();

                TotalRegistros = con.Query<int>(SQLCount).FirstOrDefault();

                con.Close();
            }
            return tags;
        }

        public List<Tag> ListarDiferenciais()
        {
            string SQL = @"SELECT 
                            ID, Nome, Cor, Descricao
                            FROM Tag
                            WHERE Diferencial = true
                            ORDER BY Nome";

            tags = new List<Tag>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                tags = con.Query<Tag>(SQL).ToList();

                con.Close();
            }
            return tags;
        }
    }
}
