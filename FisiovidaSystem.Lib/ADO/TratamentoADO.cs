﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class TratamentoADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private List<Tratamento> tratamentos;
        private Tratamento tratamento;



        public bool Excluir(int id)
        {
            string SQLTurmas = @"SELECT ID
                            FROM Turma
                            WHERE TratamentoID = @id;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                List<int> turmasIds = con.Query<int>(SQLTurmas, new { id = id }).ToList();

                if (turmasIds.Count > 0)
                {
                    con.Close();
                    return false;
                }

                string SQLDelete = @"DELETE FROM IconeModalidade WHERE TratamentoID = @id;
                        DELETE FROM ImagemModalidade WHERE TratamentoID = @id;";

                if (turmasIds.Count > 0)
                {
                    SQLDelete += @"DELETE FROM PlanoTurma WHERE TurmaID IN (" + string.Join(",", turmasIds) + @");";
                }

                SQLDelete += @"UPDATE Plano
                           SET TratamentoID = null WHERE TratamentoID = @id;
                        UPDATE PlanoLog
                           SET TratamentoID = null WHERE TratamentoID = @id;";

                SQLDelete += @"DELETE FROM Tratamento WHERE ID = @id;";

                con.Execute(SQLDelete, new { id = id });
                con.Close();
            }

            return true;
        }

        public Tratamento RegistrarImagens(List<string> arquivos, int tratamentoID)
        {
            string SQL = "";
            foreach (var arq in arquivos)
            {
                SQL += @"INSERT INTO ImagemModalidade(
                                    arquivo, tratamentoid)
                            VALUES ('" + arq + "', " + tratamentoID + ");";
            }

            string SELECT = @"SELECT Arquivo, TratamentoID FROM ImagemModalidade WHERE TratamentoID = @id;";

            tratamento = new Tratamento() { ID = tratamentoID };
            tratamento.Galeria = new List<Imagem>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL);
                tratamento.Galeria = con.Query<Imagem>(SELECT, new { id = tratamentoID }).ToList();
                con.Close();
            }
            return tratamento;
        }

        public List<Tratamento> ListarIdsNomes()
        {
            string SQL = @"SELECT 
                            ID, Nome
                           FROM Tratamento ORDER BY Nome ";

            tratamentos = new List<Tratamento>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                tratamentos = con.Query<Tratamento>(SQL).ToList();
                con.Close();
            }
            return tratamentos;
        }

        public void Inserir(Tratamento tratamento)
        {
            string SQL = @"BEGIN;
                            INSERT INTO Tratamento(nome, descricao, duracao, fotocapa, valorsessao, valorpacote)
                        VALUES (@nome, @descricao, @duracao, @FotoCapa, @valorsessao, @valorpacote);";

            foreach (var i in tratamento.TagsIds)
            {
                SQL += @"INSERT INTO IconeModalidade(TratamentoID, IconeID) VALUES ((SELECT CURRVAL('tratamento_id_seq')), " + i + ");";
            }
            SQL += "COMMIT;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, tratamento);
                con.Close();
            }
        }

        public void Atualizar(Tratamento tratamento)
        {
            string SQL = @"UPDATE Tratamento
                           SET nome=@nome, descricao=@descricao,
                                duracao=@duracao, fotocapa = @FotoCapa,
                                valorsessao = @valorsessao, valorpacote = @valorpacote
                         WHERE ID = @id;";
            string SQLTags = @"SELECT *
                                FROM Icone 
                                INNER JOIN IconeModalidade  ON IconeModalidade.IconeID = Icone.ID
                                WHERE TratamentoID = " + tratamento.ID;
            List<Icone> tagsAntigas = new List<Icone>();
            List<int> Excluir = new List<int>();
            List<int> Inserir = new List<int>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, tratamento);
                tagsAntigas = con.Query<Icone>(SQLTags).ToList();
                con.Close();
            }

            Excluir = tagsAntigas.Where(t => !tratamento.TagsIds.Any(b => b == t.ID)).Select(a => a.ID).ToList();
            Inserir = tratamento.TagsIds.Where(t => !tagsAntigas.Any(b => b.ID == t)).ToList();
            string SQLExcluir = "";
            if (Excluir.Count > 0)
            {
                SQLExcluir = @"DELETE FROM IconeModalidade WHERE IconeID IN (" + String.Join(",", Excluir) + ");";
            }

            string SQLInserir = "";
            foreach (var tag in Inserir)
            {
                SQLInserir += @" INSERT INTO IconeModalidade (IconeID, TratamentoID)
                            VALUES (" + tag + ", " + tratamento.ID + "); ";
            }

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                if (Excluir.Count > 0)
                {
                    con.Execute(SQLExcluir);
                }
                if (Inserir.Count > 0)
                {
                    con.Execute(SQLInserir);
                }
                con.Close();
            }

        }


        public Tratamento Carregar(int id)
        {
            string SQL = @"SELECT *
                            FROM Tratamento t
                            WHERE t.ID = @id";

            string SQLGaleria = @"SELECT *
                            FROM ImagemModalidade
                            WHERE TratamentoID = @id";
            string SQLIcones = @"SELECT Icone.ID, Icone.Imagem, Icone.Nome
                                FROM Icone 
                                INNER JOIN IconeModalidade  ON IconeModalidade.IconeID = Icone.ID
                                WHERE TratamentoID = @id;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                tratamento = con.Query<Tratamento>(SQL, new { id = id }).FirstOrDefault();
                tratamento.Galeria = con.Query<Imagem>(SQLGaleria, new { id = id }).ToList();
                tratamento.Icones = con.Query<Icone>(SQLIcones, new { id = id }).ToList();
                con.Close();
            }
            return tratamento;
        }

        public List<Tratamento> Listar(int skip, int take, string palavraChave)
        {
            string SQL = @"SELECT 
                            Tratamento.ID, Tratamento.Nome, Tratamento.Descricao, Tratamento.ValorSessao, 
                            Tratamento.Duracao, Tratamento.ValorPacote
                            FROM Tratamento
                            WHERE lower(Nome) like @palavraChave 
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Tratamento
                            WHERE lower(Nome) like @palavraChave ";

            tratamentos = new List<Tratamento>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                tratamentos = con.Query<Tratamento>(SQL, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return tratamentos;
        }

    }
}
