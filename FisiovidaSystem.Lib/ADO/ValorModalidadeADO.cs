﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class ValorModalidadeADO : ADOSuper
    {
        public int TotalRegistros { get; set; }

        public void Excluir(int id)
        {
            string SQL = @"DELETE FROM ValorModalidade WHERE ID = @id";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
        }

        public ValorModalidade Carregar(int id)
        {
            string SQL = @"SELECT ID, ModalidadeID, Descricao, Valor, QtdDias FROM ValorModalidade WHERE ID = @id";
            ValorModalidade vm = new ValorModalidade();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                vm = con.Query<ValorModalidade>(SQL, new { id = id }).FirstOrDefault();
                con.Close();
            }
            return vm;
        }


        public void Alterar(ValorModalidade entidade)
        {
            string SQL = @"UPDATE ValorModalidade
                            SET QtdDias = @QtdDias, Valor = @Valor, Descricao = @Descricao, ModalidadeID = @ModalidadeID
                            WHERE id = @ID";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, entidade);
                con.Close();
            }
        }

        public void Cadastrar(ValorModalidade entidade)
        {
            string SQL = @"INSERT INTO ValorModalidade
                            (QtdDias, Valor, Descricao, ModalidadeID)
                        VALUES (@QtdDias, @Valor, @Descricao, @ModalidadeID)";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, entidade);
                con.Close();
            }
        }
    }
}
