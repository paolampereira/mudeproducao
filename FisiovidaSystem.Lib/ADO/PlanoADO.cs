﻿using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Npgsql;

namespace FisiovidaSystem.Lib.ADO
{
    public class PlanoADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private List<Plano> Planos;

        public List<Plano> ListarPlanosAluno(int alunoid)
        {
            string SQL = @"SELECT 
                            Plano.ID, Plano.SaldoAulas, Plano.DataFim, Plano.DataInicio,
                            ValorModalidade.ID, ValorModalidade.Valor, ValorModalidade.QtdDias
                            FROM Plano
                            LEFT JOIN ValorModalidade ON ValorModalidade.ID = Plano.ValorModalidadeID
                            WHERE Plano.AlunoID = @id AND (Plano.DataFim IS NULL 
                                OR (extract(month from Plano.DataFim) =  @month AND 
                                    extract(year from Plano.DataFim) =  @year) );";

            Planos = new List<Plano>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Query<Plano, ValorModalidade, int>(SQL, addResultPlanos, new
                {
                    id = alunoid,
                    year = DateTime.Now.Year,
                    month = DateTime.Now.Month
                });
            }

            return Planos;
        }

        public int addResultPlanos(Plano p, ValorModalidade v)
        {
            if (p != null && v != null)
            {
                p.ValorModalidade = v;
                //se nao tem ainda na lista
                if (!Planos.Any(a => a.ID == p.ID))
                {
                    Planos.Add(p);
                }
            }

            return 1;
        }
        public void SairTurma(int turmaid, int planoid, int usuarioid)
        {
            string SQLTeste = @"SELECT v2.id
                                from valormodalidade v2
                                inner join plano on plano.id = @planoid
                                inner join valormodalidade v on v.id = plano.valormodalidadeid
                                inner join modalidade m on m.id = v.modalidadeid 
                                where v2.qtdDias = (v.qtdDias - 1) and v2.modalidadeid = m.id LIMIT 1";
            string SQL = @"BEGIN;
                            INSERT INTO planolog(
                                planoid, alunoid, professorid, datainicio, datafim, valormodalidadeid, 
                                tratamentoid, saldoaulas, dataalteracao, usuarioid)
                                select id, alunoid, professorid, datainicio, datafim, valormodalidadeid, 
                                tratamentoid, saldoaulas, now() as dataalteracao, @usuarioid as usuarioid from plano where id = @planoid;
                            DELETE FROM planoturma WHERE planoid = @planoid AND turmaid = @turmaid;
                            UPDATE Plano SET ValorModalidadeID = (SELECT v2.id
                                                    from valormodalidade v2
                                                    inner join plano on plano.id = @planoid
                                                    inner join valormodalidade v on v.id = plano.valormodalidadeid
                                                    inner join modalidade m on m.id = v.modalidadeid 
                                                    where v2.qtdDias = (v.qtdDias - 1) and v2.modalidadeid = m.id LIMIT 1) WHERE id = @planoid;
                            COMMIT;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                int id = con.Query<int>(SQLTeste, new { planoid = planoid }).FirstOrDefault();
                if (id == 0)
                {
                    con.Close();
                    throw new Exception("Não há planos disponíveis com um dia a menos.");
                }
                con.Execute(SQL, new { turmaid = turmaid, planoid = planoid, usuarioid = usuarioid });
                con.Close();
            }

        }

        public void CancelarPlano(int planoID)
        {
            string SQL = @"BEGIN;
                                DELETE FROM InscricaoTemp WHERE planoid = @id;
                                DELETE FROM PlanoTurma WHERE planoid = @id 
                                    AND turmaid IN (SELECT Turma.ID FROM Turma 
                                                    INNER JOIN PlanoTurma ON PlanoTurma.TurmaID = Turma.ID
                                                    INNER JOIN Plano ON PlanoTurma.PlanoID = Plano.ID
                                                    WHERE Plano.ID = @id) ;
                                UPDATE Plano SET datafim = @DataFim WHERE ID = @id; 
                            COMMIT;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = planoID, DataFim = DateTime.Now });
                con.Close();
            }

        }


        public void RealizarMatricula(Plano plano)
        {

            string SQL = @"BEGIN; 
                            INSERT INTO Plano(alunoid, professorid, datainicio, valormodalidadeid, saldoaulas)
                    VALUES (@AlunoID, @ProfessorID, @DataInicio, @ValorModalidadeID, @SaldoAulas);";

            foreach (var turma in plano.TurmasIDs)
            {
                SQL += @"INSERT INTO PlanoTurma (planoid, turmaid)
                        VALUES ((SELECT CURRVAL('plano_id_seq')), " + turma + "); ";
            }
            SQL += @"COMMIT;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, plano);
                con.Close();
            }

        }
    }
}
