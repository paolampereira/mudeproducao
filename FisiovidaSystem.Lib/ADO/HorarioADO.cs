﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class HorarioADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private List<Horario> horarios;
        private Horario horario;

        public List<Horario> ListarHorarios()
        {
            string SQL = @"SELECT 
                            Horario.ID, Horario.Hora, Horario.Inativo, Horario.DiaSemana,
                            Horario.DataCadastro, Horario.UsuarioID, Horario.DataModificacao
                            FROM Horario
                            ORDER BY DiaSemana, Hora";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Horario ";


            horarios = new List<Horario>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                horarios = con.Query<Horario>(SQL).ToList();

                TotalRegistros = con.Query<int>(SQLCount).FirstOrDefault();

                con.Close();
            }
            return horarios;
        }

        public bool Excluir(int id)
        {
            string SQL = @"SELECT COUNT(*) FROM Turma WHERE Horarioid = @id;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                TotalRegistros = con.Query<int>(SQL, new { id = id }).FirstOrDefault();

                con.Close();
            }
            if (TotalRegistros > 0)
            {
                return false;
            }
            SQL = @"DELETE FROM Horario WHERE id = @id;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
            return true;
        }

        public List<Horario> Listar(int skip, int take)
        {
            string SQL = @"SELECT 
                            Horario.ID, Horario.Hora, Horario.Inativo, Horario.DiaSemana,
                            Horario.DataCadastro, Horario.UsuarioID, Horario.DataModificacao,
                            Usuario.ID, Usuario.Nome
                            FROM Horario
                            INNER JOIN Usuario ON Usuario.ID = Horario.UsuarioID
                            ORDER BY DiaSemana, Hora
                            OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Horario ";


            horarios = new List<Horario>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Horario, Usuario, Horario>(SQL, addResultHorarios, new
                {
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount).FirstOrDefault();

                con.Close();
            }
            return horarios;
        }
        public Horario addResultHorarios(Horario horario, Usuario usuario)
        {
            horario.Usuario = usuario;
            horarios.Add(horario);
            return horario;
        }
        public void Atualizar(Horario horario)
        {
            string SQL = @"UPDATE Horario
                           SET hora = @Hora, diasemana= @DiaSemana,
                               datamodificacao=@DataModificacao, usuarioid = @UsuarioID
                         WHERE id = @ID ";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, horario);
                con.Close();
            }
        }

        public void Salvar(Horario horario)
        {
            string SQL = @"INSERT INTO Horario
                            (DiaSemana, Hora, Inativo, UsuarioID)
                        VALUES (@DiaSemana, @Hora, false, @UsuarioID)";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, horario);
                con.Close();
            }

        }

        public Horario Carregar(int id)
        {
            horario = new Horario();
            string SQL = @"SELECT 
                            *
                            FROM Horario
                            WHERE ID = @id";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                horario = con.Query<Horario>(SQL, new { ID = id }).FirstOrDefault();
                con.Close();
            }
            return horario;
        }
    }
}
