﻿using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Dapper;
using Npgsql;

namespace FisiovidaSystem.Lib.ADO
{
    public class UsuarioADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        public List<Usuario> Usuarios { get; set; }

        public List<Usuario> ListaProfessoresModalidade(int modalidadeID, int tratamentoID)
        {
            string SQL = @"SELECT 
                            ID, Nome, Login, Master, Inativo, Email, Professor, CPF, Foto, DescricaoProfissional
                            FROM Usuario
                            WHERE (SELECT COUNT(*) FROM Turma 
                                    WHERE ProfessorID = Usuario.ID AND ModalidadeID = " + modalidadeID + ") > 0 AND Foto IS NOT NULL;";
            if (tratamentoID > 0)
            {
                SQL = @"SELECT 
                            ID, Nome, Login, Master, Inativo, Email, Professor, CPF, Foto, DescricaoProfissional
                            FROM Usuario
                            WHERE (SELECT COUNT(*) FROM Turma 
                                    WHERE ProfessorID = Usuario.ID AND TratamentoID = " + tratamentoID + ") > 0 AND Foto IS NOT NULL;";
            }
            Usuarios = new List<Usuario>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                Usuarios = con.Query<Usuario>(SQL).ToList();
                con.Close();
            }
            return Usuarios;
        }

        public Usuario Carregar(int id)
        {
            string SQL = @"SELECT 
                            ID, Nome, Login, Senha, Master, Inativo, Email, Professor, CPF, Foto, DescricaoProfissional
                            FROM Usuario
                            WHERE ID = @id";
            Usuario u = new Usuario();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                u = con.Query<Usuario>(SQL, new { ID = id }).FirstOrDefault();
                con.Close();
            }
            return u;
        }

        public List<Usuario> CarregarProfessoresTurmas(int valorModalidadeID)
        {
            string SQL = @"SELECT 
                        Usuario.ID, Usuario.Nome,
                        Turma.id, Turma.ProfessorID, Turma.Modalidadeid, 
                        (select count(*) from PlanoTurma 
                            inner join plano on plano.ID = planoturma.planoid 
                            where PlanoTurma.TurmaID = Turma.ID and plano.DataFim IS NULL) as VagasOcupadas,
                        Horario.ID, Horario.Hora, Horario.DiaSemana,
                        Modalidade.ID, Modalidade.VagasPorTurma
                        FROM Usuario
                        INNER JOIN Turma ON Turma.ProfessorID = Usuario.ID
                        INNER JOIN Horario ON Turma.HorarioID = Horario.ID
                        INNER JOIN Modalidade ON Turma.ModalidadeID = Modalidade.ID
                        WHERE Usuario.Professor = true 
                        and
                        Turma.modalidadeid IN 
                        (select Modalidade.ID from Modalidade  
	                    INNER JOIN ValorModalidade ON Modalidade.id = ValorModalidade.modalidadeID 
	                    WHERE ValorModalidade.ID = @id)";

            Usuario u = new Usuario();
            Usuarios = new List<Usuario>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Usuario, Turma, Horario, Modalidade, int>(SQL, addResultProfessor, new { id = valorModalidadeID });
                con.Close();
            }
            return Usuarios;
        }

        public int addResultProfessor(Usuario professor, Turma turma, Horario horario, Modalidade modalidade)
        {
            Usuario u = Usuarios.FirstOrDefault(p => p.ID == professor.ID);

            turma.Horario = horario;
            turma.Modalidade = modalidade;

            if (u == null)
            {
                professor.Turmas = new List<Turma>();
                professor.Turmas.Add(turma);
                Usuarios.Add(professor);
            }
            else
            {
                u.Turmas.Add(turma);
            }

            return 1;
        }

        public Usuario Carregar(string login)
        {
            string SQL = @"SELECT 
                            ID, Nome, Login, Senha, Master, Inativo, Email, Professor, CPF, Foto
                            FROM Usuario
                            WHERE Login = @login";

            Usuario u = new Usuario();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                u = con.Query<Usuario>(SQL, new { login = login }).FirstOrDefault();
                con.Close();
            }
            return u;
        }

        public bool Excluir(int id)
        {
            string SQLTurmas = @"SELECT COUNT(*) FROM Turma WHERE ProfessorID = @id";
            int cont = 0;


            string SQLPlanos = @"SELECT COUNT(*) FROM Plano WHERE ProfessorID = @id";
            int cont2 = 0;

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                cont = con.Query<int>(SQLTurmas, new
                {
                    id = id
                }).FirstOrDefault();

                cont2 = con.Query<int>(SQLPlanos, new
                {
                    id = id
                }).FirstOrDefault();

                con.Close();
            }
            if (cont > 0 || cont2 > 0)
            {
                return false;
            }

            string SQL = @"UPDATE planolog
                           SET usuarioid=NULL
                         WHERE UsuarioID = @id;

                            UPDATE Novidade
                           SET usuarioid=NULL
                         WHERE UsuarioID = @id;

                            UPDATE Horario
                           SET usuarioid=NULL
                         WHERE UsuarioID = @id;
                        DELETE FROM Usuario WHERE ID = @id;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                cont = con.Query<int>(SQL, new
                {
                    id = id
                }).FirstOrDefault();

                con.Close();
            }

            return true;
        }

        public Usuario Logar(string login, string senha)
        {
            string SQL = @"SELECT 
                            ID, Nome, Login, Master, Inativo, Email, Professor, CPF, Foto
                            FROM Usuario
                            WHERE Login = @login AND Senha = md5(@senha);";

            Usuario u = new Usuario();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                u = con.Query<Usuario>(SQL, new
                {
                    login = login,
                    senha = senha
                }).FirstOrDefault();
                con.Close();
            }
            return u;
        }

        public List<Usuario> ListarProfessores()
        {
            string SQL = @"SELECT 
                            ID, Nome, Foto, DescricaoProfissional
                            FROM Usuario
                            WHERE Professor = true AND Foto IS NOT NULL
                            ORDER BY Nome;";

            List<Usuario> usuarios = new List<Usuario>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                usuarios = con.Query<Usuario>(SQL).ToList();

                con.Close();
            }
            return usuarios;
        }

        public List<Usuario> Listar(int skip, int take, string palavraChave)
        {
            string SQL = @"SELECT 
                            ID, Nome, Login, Senha, Master, Inativo, Email, Professor, CPF, Foto
                            FROM Usuario
                            WHERE lower(Nome) like @palavraChave 
                            ORDER BY Nome
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Usuario
                            WHERE lower(Nome) like @palavraChave ";
            List<Usuario> usuarios = new List<Usuario>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                usuarios = con.Query<Usuario>(SQL, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return usuarios;
        }
        public List<Usuario> ListarIdsNomesProfessores()
        {
            string SQL = @"SELECT 
                            ID, Nome
                            FROM Usuario 
                            WHERE Professor = true 
                            ORDER BY Nome";

            List<Usuario> usuarios = new List<Usuario>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                usuarios = con.Query<Usuario>(SQL).ToList();
                con.Close();
            }
            return usuarios;
        }
        public void Atualizar(Usuario usuario)
        {
            string SQL = @"UPDATE Usuario
                           SET login=@Login, nome=@Nome, cpf=@CPF, 
                               foto=@Foto, email=@Email, inativo=@Inativo,
                               descricaoprofissional=@DescricaoProfissional
                         WHERE ID = @ID";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, usuario);
                con.Close();
            }
        }
        public void Inserir(Usuario usuario)
        {
            string SQL = @" INSERT INTO Usuario (Nome, Login, Senha, Master, Inativo, Email, Professor, CPF, Foto, DescricaoProfissional, Sexo) 
                                 VALUES (@Nome, @Login, md5(@Senha), @Master, @Inativo, @Email, @Professor, @CPF, @Foto, @DescricaoProfissional, @Sexo) ";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, usuario);
                con.Close();
            }
        }
        public void AlterarIsProfessor(Usuario usuario)
        {
            string SQL = @" UPDATE Usuario SET
                                 Professor = @Professor
                                 WHERE id = @ID";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, usuario);
                con.Close();
            }
        }
        public void AlterarIsMaster(Usuario usuario)
        {
            string SQL = @" UPDATE Usuario SET
                                 Master = @Master
                                 WHERE id = @ID";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, usuario);
                con.Close();
            }
        }

        public void AlterarSenha(Usuario usuario)
        {
            string SQL = @" UPDATE Usuario SET
                                 Senha = md5(@Senha)
                                 WHERE id = @ID";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, usuario);
                con.Close();
            }
        }
    }
}
