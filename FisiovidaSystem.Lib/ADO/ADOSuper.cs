﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.ADO
{
    public abstract class ADOSuper
    {
        protected Database _db;

        public NpgsqlConnection CreateConnection()
        {
            return new NpgsqlConnection("SERVER=localhost; PORT=5432; USER ID=admin; PASSWORD=admin; DATABASE=Fisiovida");
            //return new NpgsqlConnection("SERVER=187.86.152.182; PORT=5432; USER ID=muder_user; PASSWORD=bNqj12$8; DATABASE=mudergco_site");
        }

        static ADOSuper()
        {
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
        }

        public ADOSuper()
        {
            //_db = DatabaseFactory.CreateDatabase("fisiovida");
        }

        private string ChaveCriptografia
        {
            set { }
            get { return "ch@ve"; }
        }

        public string Criptografar(string valor)
        {

            SqlParameter p = new SqlParameter("@a", valor);
            return " ENCRYPTBYPASSPHRASE('" + ChaveCriptografia + "', '" + p.SqlValue + "') ";
        }

        public string Decriptografar(string nome)
        {
            return " CONVERT(VARCHAR(MAX), DECRYPTBYPASSPHRASE('" + ChaveCriptografia + "', " + nome + ")) ";
        }
    }

    public class EnumStringTypeHandler<T> : SqlMapper.TypeHandler<T>
    {
        public override T Parse(object value)
        {
            if (value == null || value is DBNull) { return default(T); }
            return (T)Enum.Parse(typeof(T), value.ToString());
        }

        public override void SetValue(IDbDataParameter parameter, T value)
        {
            parameter.DbType = DbType.String;
            parameter.Value = value.ToString();
        }
    }
}
