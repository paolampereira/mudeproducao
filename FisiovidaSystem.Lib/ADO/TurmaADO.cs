﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class TurmaADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private List<Turma> turmas;
        private Turma turma;

        public List<Turma> Listar(int skip, int take, int diaSemana, int professorID)
        {
            string SQL = @"SELECT 
                            Turma.ID,  
                            (SELECT COUNT(*) FROM PlanoTurma 
                                inner join plano on plano.ID = planoturma.planoid 
                                where PlanoTurma.TurmaID = Turma.ID and plano.DataFim IS NULL) AS VagasOcupadas,
                            Horario.ID, Horario.Hora, Horario.DiaSemana,
                            Usuario.ID, Usuario.Nome,
                            Modalidade.ID, Modalidade.Nome, Modalidade.VagasPorTurma,
                            Tratamento.ID, Tratamento.Nome
                            FROM Turma
                            INNER JOIN Horario ON Horario.ID = Turma.HorarioID
                            INNER JOIN Usuario ON Usuario.ID = Turma.ProfessorID
                            LEFT JOIN Modalidade ON Modalidade.ID = Turma.ModalidadeID
                            LEFT JOIN Tratamento ON Tratamento.ID = Turma.TratamentoID";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Turma
                            INNER JOIN Horario ON Horario.ID = Turma.HorarioID
                            INNER JOIN Usuario ON Usuario.ID = Turma.ProfessorID
                            LEFT JOIN Modalidade ON Modalidade.ID = Turma.ModalidadeID
                            LEFT JOIN Tratamento ON Tratamento.ID = Turma.TratamentoID";

            if (diaSemana >= 0)
            {
                SQL += @" WHERE Horario.DiaSemana = @dia ";
                SQLCount += @" WHERE Horario.DiaSemana = @dia ";
            }
            if (professorID > 0)
            {
                if (diaSemana < 0)
                {
                    SQL += @" WHERE ";
                    SQLCount += @" WHERE ";
                }
                else
                {
                    SQL += " AND ";
                    SQLCount += " AND ";
                }
                SQL += @" Usuario.ID = @professorid ";
                SQLCount += @" Usuario.ID = @professorid ";
            }
            /*if (modalidadeID > 0)
            {
                if (diaSemana < 0 && professorID == 0)
                {
                    SQL += @" WHERE ";
                    SQLCount += @" WHERE ";
                }
                else
                {
                    SQL += " AND ";
                    SQLCount += " AND ";
                }
                SQL += @" Modalidade.ID = @modalidadeid ";
                SQLCount += @" Modalidade.ID = @modalidadeid ";
            }*/

            SQL += @" ORDER BY Modalidade.Nome, Tratamento.Nome, Horario.DiaSemana, Horario.Hora OFFSET @skip LIMIT @take";


            turmas = new List<Turma>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Turma, Horario, Usuario, Modalidade, Tratamento, int>(SQL, addResultTurma, new
                {
                    professorid = professorID,
                    dia = diaSemana,
                    //modalidadeid = modalidadeID,
                    skip = skip,
                    take = take
                });

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    professorid = professorID,
                    dia = diaSemana,
                    //modalidadeid = modalidadeID,
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return turmas;
        }
        public int addResultTurma(Turma turma, Horario horario, Usuario usuario, Modalidade modalidade, Tratamento tratamento)
        {
            turma.Professor = usuario;
            turma.Horario = horario;
            turma.Modalidade = modalidade;
            turma.Tratamento = tratamento;
            turmas.Add(turma);

            return 1;
        }

        public bool Excluir(int id)
        {
            string SQL = @"DELETE FROM PlanoTurma WHERE TurmaID = @id;
                           DELETE FROM InscricaoTemp  WHERE TurmaID = @id;
                           DELETE FROM Turma WHERE ID = @id;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
            return true;
        }

        public Turma Carregar(int id)
        {
            string SQL = @"SELECT 
                            Turma.ID, 
                            (SELECT COUNT(*) FROM PlanoTurma 
                                WHERE PlanoTurma.TurmaID = @id) AS VagasOcupadas,
                            Modalidade.ID, Modalidade.Nome, Modalidade.VagasPorTurma, 
                            Tratamento.ID, Tratamento.Nome,
                            Horario.ID, Horario.Hora, Horario.DiaSemana,
                            Usuario.ID, Usuario.Nome,
                            Aluno.ID, Aluno.Nome
                            FROM Turma
                            LEFT JOIN PlanoTurma ON PlanoTurma.TurmaID = Turma.ID
                            LEFT JOIN Plano ON PlanoTurma.PlanoID = Plano.ID
                            LEFT JOIN Aluno ON Aluno.ID = Plano.AlunoID
                            LEFT JOIN Modalidade ON Modalidade.ID = Turma.ModalidadeID
                            LEFT JOIN Tratamento ON Tratamento.ID = Turma.TratamentoID
                            INNER JOIN Horario ON Horario.ID = Turma.HorarioID
                            INNER JOIN Usuario ON Usuario.ID = Turma.ProfessorID 
                            WHERE Turma.ID = @id";
            turmas = new List<Turma>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Turma, Modalidade, Tratamento, Horario, Usuario,Aluno, int>(SQL, addResultDetalheTurma, new { id = id });
                con.Close();
            }
            return turmas.First();
        }
        public int addResultDetalheTurma(Turma turma, Modalidade modalidade, Tratamento tratamento, Horario horario, Usuario usuario, Aluno aluno)
        {
            if (turmas.Count == 0)
            {
                turma.Professor = usuario;
                turma.Horario = horario;
                turma.Modalidade = modalidade;
                turma.Tratamento = tratamento;
                turma.Alunos = new List<Aluno>();
                turmas.Add(turma);
            }

            if (aluno != null)
            {
                turmas.First().Alunos.Add(aluno);
            }

            return 1;
        }

        public void Cadastrar(Turma turma)
        {
            string SQL = @"INSERT INTO Turma
                            (horarioid, modalidadeid, professorid, inativo, tratamentoid)
                        VALUES (@horarioid, @modalidadeid, @professorid, @inativo, NULL);";

            if (turma.TratamentoID > 0)
            {
                SQL = @"INSERT INTO Turma
                            (horarioid, professorid, inativo, tratamentoid)
                        VALUES (@horarioid, @professorid, @inativo, @tratamentoid);";
            }
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, turma);
                con.Close();
            }
        }
    }
}
