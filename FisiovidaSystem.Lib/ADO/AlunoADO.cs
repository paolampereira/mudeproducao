﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class AlunoADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private Aluno Aluno;
        private List<Aluno> alunos;

        public void Excluir(int id)
        {
            string SQLPlanosIds = @"SELECT ID FROM Plano WHERE AlunoID = @id;";
            string SQLMensalidadesIds = @"SELECT ID FROM Mensalidade WHERE AlunoID = @id;";
            string SQLDelete = "";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                List<int> planosIds = con.Query<int>(SQLPlanosIds, new { ID = id }).ToList();
                List<int> mensaIds = con.Query<int>(SQLMensalidadesIds, new { ID = id }).ToList();
                if (planosIds.Count > 0)
                {
                    SQLDelete += @"DELETE FROM PlanoTurma WHERE PlanoID IN (" + string.Join(",", planosIds) + @");
                                DELETE FROM PlanoMensalidade WHERE PlanoID IN (" + string.Join(",", planosIds) + ");";
                }
                if (mensaIds.Count > 0)
                {
                    SQLDelete += @"DELETE FROM DescontoMensalidade WHERE MensalidadeID IN (" + string.Join(",", mensaIds) + ");";
                }
                SQLDelete += @"DELETE FROM InscricaoTemp WHERE AlunoID = @id;
                               DELETE FROM PlanoLog WHERE AlunoID = @id;
                               DELETE FROM Plano WHERE AlunoID = @id;
                               DELETE FROM Mensalidade WHERE AlunoID = @id;
                               DELETE FROM Aluno WHERE ID = @id;";
                con.Execute(SQLDelete, new { ID = id });
                con.Close();
            }

        }

        public Aluno Carregar(int id)
        {
            string SQL = @"SELECT 
                            Aluno.ID, Aluno.Nome, Aluno.Login, Aluno.Senha, Aluno.Email, Aluno.CPF, Aluno.Foto, Aluno.DataNascimento, Aluno.Telefone, Aluno.Telefone2, Aluno.Sexo,
                            Plano.ID, Plano.SaldoAulas, Plano.DataFim, Plano.DataInicio,
                            ValorModalidade.ID, ValorModalidade.Valor, ValorModalidade.QtdDias,
                            Modalidade.ID, Modalidade.Nome, Modalidade.VagasPorTurma, Modalidade.Duracao,
                            Turma.ID, Turma.ProfessorID, Turma.HorarioID, PlanoTurma.PlanoID AS PlanoID,
                            Horario.ID, Horario.Hora, Horario.DiaSemana,
                            Usuario.ID, Usuario.Nome
                            FROM Aluno
                            LEFT JOIN Plano ON Aluno.ID = Plano.AlunoID
                            LEFT JOIN ValorModalidade ON ValorModalidade.ID = Plano.ValorModalidadeID
                            LEFT JOIN Modalidade ON Modalidade.ID = ValorModalidade.ModalidadeID
                            LEFT JOIN PlanoTurma ON PlanoTurma.PlanoID = Plano.ID
                            LEFT JOIN Turma ON PlanoTurma.TurmaID = Turma.ID
                            LEFT JOIN Horario ON Turma.HorarioID = Horario.ID
                            LEFT JOIN Usuario ON Usuario.ID = Turma.ProfessorID
                            WHERE Aluno.ID = @id";
            //mesmo que não tenha turma na modalidade, tem que listar a modalidade pois terá um aviso..
            Aluno = new Aluno();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Aluno, Plano, ValorModalidade, Modalidade, Turma, Horario, Usuario, Aluno>(SQL, addResultAluno, new { ID = id });
                con.Close();
            }
            return Aluno;
        }

        public Aluno CarregarDados(int id)
        {
            string SQL = @"SELECT 
                            Aluno.ID, Aluno.Nome, Aluno.Login, Aluno.Senha, Aluno.Email, Aluno.CPF, 
                            Aluno.Foto, Aluno.DataNascimento, Aluno.Telefone, Aluno.Telefone2, Aluno.Sexo
                            FROM Aluno
                            WHERE Aluno.ID = @id";
            //mesmo que não tenha turma na modalidade, tem que listar a modalidade pois terá um aviso..
            Aluno = new Aluno();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                Aluno = con.Query<Aluno>(SQL, new { ID = id }).FirstOrDefault();
                con.Close();
            }
            return Aluno;
        }

        public Aluno Cadastrar(Aluno aluno)
        {
            string SQL = @"INSERT INTO ALUNO
                            (login, senha, sexo, nome, foto, email, inativo, cpf, datanascimento, telefone, telefone2)
                        VALUES (@login, @senha, @sexo, @nome, @foto, @email, @inativo, @cpf, @datanascimento, @telefone, @telefone2)";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, aluno);
                con.Close();
            }
            return Aluno;
        }

        public void Editar(Aluno aluno)
        {
            string SQL = @"UPDATE Aluno
                           SET login=@Login, nome=@Nome, email=@Email, cpf=@CPF, 
                               datamodificacao=@DataModificacao, inativo=@Inativo, datanascimento=@DataNascimento, 
                               telefone=@Telefone, telefone2=@Telefone2, 
                               sexo=@Sexo
                         WHERE id = @ID ";

            aluno.DataModificacao = DateTime.Now;
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, aluno);
                con.Close();
            }
        }

        public Aluno addResultAluno(Aluno alunoRetorno, Plano plano, ValorModalidade valorModalidade,
            Modalidade modalidade, Turma turma, Horario horario, Usuario professor)
        {
            if (Aluno.ID <= 0)
            {
                Aluno = alunoRetorno;
                Aluno.Cancelamentos = new List<InscricaoTemp>();
                Aluno.Recuperacoes = new List<InscricaoTemp>();
            }

            if (Aluno.Planos == null)
            {
                Aluno.Planos = new List<Plano>();
            }

            if (plano != null) // Se o plano não está nulo
            {
                if (!Aluno.Planos.Any(p => p.ID == plano.ID)) // se o plano nao foi adicionado nos planos do aluno
                {
                    valorModalidade.Modalidade = modalidade;
                    plano.ValorModalidade = valorModalidade;

                    plano.TurmasPlano = new List<Turma>();
                    if (turma != null)
                    {
                        turma.Horario = horario;
                        turma.Professor = professor;
                        plano.TurmasPlano.Add(turma);
                        plano.Professor = professor;
                    }
                    Aluno.Planos.Add(plano);
                }
                else
                {
                    Plano p = Aluno.Planos.FirstOrDefault(a => a.ID == plano.ID);
                    if (turma != null)
                    {
                        if (!p.TurmasPlano.Any(t => t.ID == turma.ID))
                        {
                            turma.Horario = horario;
                            turma.Professor = professor;
                            p.TurmasPlano.Add(turma);
                        }
                    }
                }
            }

            return Aluno;
        }

        public Aluno Carregar(string login)
        {
            string SQL = @"SELECT 
                            ID, Nome, Login, Senha, Email, CPF, Foto
                            FROM Aluno
                            WHERE Login = @login";
            Aluno retorno = new Aluno();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                retorno = con.Query<Aluno>(SQL, new { login = login }).FirstOrDefault();
                con.Close();
            }
            return retorno;
        }

        public List<Aluno> Listar(int skip, int take, string palavraChave)
        {
            string SQL = @"SELECT 
                            Aluno.ID, Aluno.Nome, Aluno.Login, Aluno.Senha, Aluno.Email, Aluno.CPF, Aluno.Foto, Aluno.Telefone, Aluno.Telefone2
                            FROM Aluno
                            WHERE lower(Nome) like @palavraChave OR cpf like @palavraChave
                            ORDER BY Aluno.Nome
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Aluno
                            WHERE lower(Nome) like @palavraChave ";
            alunos = new List<Aluno>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                Aluno = new Aluno();
                alunos = con.Query<Aluno>(SQL, new
                {
                    palavraChave = "%" + palavraChave + "%",
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave + "%",
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return alunos;
        }

        public void Inserir(Aluno entidade)
        {
            string SQL = @" INSERT INTO Aluno (Nome, Login, Senha, Email, CPF, Foto) 
                                 VALUES (@Nome, @Login, @Senha, @Email, @CPF, @Foto) ";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, entidade);
                con.Close();
            }
        }
    }
}
