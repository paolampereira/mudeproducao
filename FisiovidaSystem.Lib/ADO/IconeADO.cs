﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class IconeADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private List<Icone> icones;

        public void Excluir(int id)
        {
            string SQL = @" DELETE FROM IconeModalidade WHERE IconeID = @id; 
                            DELETE FROM Icone WHERE ID = @id;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
        }
        public void Cadastrar(Icone icone)
        {
            string SQL = @"INSERT INTO Icone
                            (Nome, Imagem)
                        VALUES (@Nome, @Imagem)";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, icone);
                con.Close();
            }
        }

        public void Alterar(Icone icone)
        {
            string SQL = @"UPDATE Icone
                           SET nome = @Nome, Imagem = @Imagem
                         WHERE id = @ID";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, icone);
                con.Close();
            }
        }

        public Icone Carregar(int id)
        {
            string SQL = @"SELECT 
                            ID, Nome, Imagem
                            FROM Icone
                            WHERE id = @id";

            Icone icone = new Icone();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                icone = con.Query<Icone>(SQL, new
                {
                    id = id
                }).FirstOrDefault();

                con.Close();
            }
            return icone;
        }

        public List<Icone> Listar(int skip, int take, string palavraChave)
        {
            string SQL = @"SELECT 
                            ID, Nome, Imagem
                            FROM Icone
                            WHERE lower(Nome) like @palavraChave
                            ORDER BY Nome
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Icone
                            WHERE lower(Nome) like @palavraChave";
            icones = new List<Icone>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                icones = con.Query<Icone>(SQL, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return icones;
        }

        public List<Icone> Listar()
        {
            string SQL = @"SELECT 
                            ID, Nome, Imagem
                            FROM Icone
                            ORDER BY Nome";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Icone";

            icones = new List<Icone>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                icones = con.Query<Icone>(SQL).ToList();

                TotalRegistros = con.Query<int>(SQLCount).FirstOrDefault();

                con.Close();
            }
            return icones;
        }
    }
}
