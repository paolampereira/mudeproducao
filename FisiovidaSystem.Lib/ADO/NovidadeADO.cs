﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class NovidadeADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private List<Novidade> novidades;

        public List<Novidade> Listar(int take, int skip)
        {
            string SQLids = @"SELECT 
                            Novidade.ID
                            FROM Novidade
                            ORDER BY DataCadastro DESC, Titulo
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Novidade";

            novidades = new List<Novidade>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                List<int> ids = con.Query<int>(SQLids, new
                {
                    skip = skip,
                    take = take
                }).ToList();

                string[] a = ids.Select(x => x.ToString()).ToArray();
                string SQL = @"SELECT 
                            Novidade.ID, Novidade.Titulo, Novidade.Texto, Novidade.Imagem,Novidade.ImagemDestaque,Novidade.ImagemCapa, Novidade.Destaque, 
                            Novidade.DataModificacao, Novidade.DataCadastro, Novidade.UsuarioID,
                            Tag.ID, Tag.Nome, Tag.Cor
                            FROM Novidade
                            LEFT JOIN TagNovidade ON TagNovidade.NovidadeID = Novidade.ID 
                            LEFT JOIN Tag ON Tag.ID = TagNovidade.TagID
                            WHERE Novidade.ID IN (" + string.Join(",", a) + @")
                            ORDER BY DataCadastro DESC, Titulo;";
                con.Query<Novidade, Tag, int>(SQL, addResultNovidade).ToList();
                TotalRegistros = con.Query<int>(SQLCount).FirstOrDefault();

                con.Close();
            }
            return novidades;
        }
        public int addResultNovidade(Novidade novidade, Tag tag)
        {
            if (novidade != null)
            {
                if (!novidades.Any(n => n.ID == novidade.ID))
                {
                    novidade.Tags = new List<Tag>();
                    if (tag != null)
                    {
                        novidade.Tags.Add(tag);
                    }
                    novidades.Add(novidade);
                }
                else
                {
                    Novidade n = novidades.FirstOrDefault(a => a.ID == novidade.ID);
                    if (tag != null)
                    {
                        n.Tags.Add(tag);
                    }
                }
            }

            return 1;
        }
        public void Excluir(int id)
        {
            string SQL = @" DELETE FROM GaleriaNovidade WHERE NovidadeID = @id; 
                            DELETE FROM TagNovidade WHERE NovidadeID = @id; 
                            DELETE FROM Novidade WHERE ID = @id; ";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
        }

        public void RemoverImagem(int id)
        {
            string SQL = @"DELETE FROM GaleriaNovidade WHERE ID = @id";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
        }

        public void Cadastrar(Novidade novidade)
        {
            string SQL = @" BEGIN; 
                            INSERT INTO Novidade
                            (Titulo, Texto, Imagem, ImagemDestaque, ImagemCapa, Destaque, DataModificacao, DataCadastro, UsuarioID, Chamada)
                        VALUES (@Titulo, @Texto, @Imagem, @ImagemDestaque, @ImagemCapa, @Destaque, @DataModificacao, @DataCadastro, @UsuarioID, @Chamada); ";
            foreach (var t in novidade.TagsIds)
            {
                SQL += @" INSERT INTO TagNovidade (TagID, NovidadeID)
                            VALUES (" + t + ", (SELECT CURRVAL('novidade_id_seq'))); ";
            }

            SQL += "COMMIT;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, novidade);
                con.Close();
            }


        }

        public void Alterar(Novidade novidade)
        {
            string SQL = @"UPDATE Novidade
                           SET Titulo = @Titulo, Texto = @Texto, Imagem = @Imagem, Destaque = @Destaque, Chamada = @Chamada,
                            DataModificacao = @DataModificacao, UsuarioID = @UsuarioID, ImagemDestaque = @ImagemDestaque, ImagemCapa = @ImagemCapa
                         WHERE id = @ID";

            string SQLTags = @"SELECT 
                            ID, Nome, Cor
                            FROM Tag
                            WHERE ID IN (SELECT TagID FROM TagNovidade WHERE NovidadeID = " + novidade.ID + @")
                            ORDER BY Nome";
            List<Tag> tagsAntigas = new List<Tag>();
            List<int> Excluir = new List<int>();
            List<int> Inserir = new List<int>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, novidade);
                tagsAntigas = con.Query<Tag>(SQLTags).ToList();

                con.Close();
            }

            Excluir = tagsAntigas.Where(t => !novidade.TagsIds.Any(b => b == t.ID)).Select(a => a.ID).ToList();
            Inserir = novidade.TagsIds.Where(t => !tagsAntigas.Any(b => b.ID == t)).ToList();
            string SQLExcluir = "";
            if (Excluir.Count > 0)
            {
                SQLExcluir = @"DELETE FROM TagNovidade WHERE TagID IN (" + String.Join(",", Excluir) + ");";
            }

            string SQLInserir = "";
            foreach (var tag in Inserir)
            {
                SQLInserir += @" INSERT INTO TagNovidade (TagID, NovidadeID)
                            VALUES (" + tag + ", " + novidade.ID + "); ";
            }

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                if (Excluir.Count > 0)
                {
                    con.Execute(SQLExcluir);
                }
                if (Inserir.Count > 0)
                {
                    con.Execute(SQLInserir);
                }
                con.Close();
            }
        }

        public void RegistrarImagens(List<string> imagens, int novidadeID)
        {
            string SQL = "";
            foreach (var arq in imagens)
            {
                SQL += @"INSERT INTO GaleriaNovidade(
                                    Arquivo, NovidadeID)
                            VALUES ('" + arq + "', " + novidadeID + ");";
            }

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL);
                con.Close();
            }
        }

        public List<Novidade> ListarDestaques()
        {
            string SQL = @"SELECT 
                            *
                            FROM Novidade
                            WHERE Destaque = true
                            ORDER BY DataCadastro, Titulo;";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Novidade
                            WHERE Destaque = true;";

            List<Novidade> novidades = new List<Novidade>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                novidades = con.Query<Novidade>(SQL).ToList();
                TotalRegistros = con.Query<int>(SQLCount).FirstOrDefault();

                con.Close();
            }
            return novidades;
        }

        public Novidade Carregar(int id)
        {
            string SQL = @"SELECT 
                            ID, Titulo, Texto, Imagem, ImagemCapa, ImagemDestaque, Destaque, DataModificacao, DataCadastro, UsuarioID, Chamada
                            FROM Novidade
                            WHERE id = @id";

            Novidade novidade = new Novidade();
            novidade.Galeria = new List<Imagem>();

            string SQLTags = @"SELECT 
                            ID, Nome, Cor
                            FROM Tag
                            WHERE ID IN (SELECT TagID FROM TagNovidade WHERE NovidadeID = " + id + @")
                            ORDER BY Nome";

            string SQLGaleria = @"SELECT 
                            ID, NovidadeID, Arquivo
                            FROM GaleriaNovidade
                            WHERE NovidadeID = @id";

            string SQLUsuario = @"SELECT *
                            FROM Usuario
                            WHERE ID = @id";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                novidade = con.Query<Novidade>(SQL, new
                {
                    id = id
                }).FirstOrDefault();

                novidade.Tags = con.Query<Tag>(SQLTags).ToList();

                novidade.Galeria = con.Query<Imagem>(SQLGaleria, new
                {
                    id = id
                }).ToList();

                if (novidade.UsuarioID != 0)
                {
                    novidade.Usuario = con.Query<Usuario>(SQLUsuario, new
                    {
                        id = novidade.UsuarioID
                    }).FirstOrDefault();
                }

                con.Close();
            }
            return novidade;
        }

        public List<Novidade> Listar(int skip, int take, string palavraChave)
        {
            string SQL = @"SELECT 
                            ID, Titulo, Texto, Imagem, Destaque, DataModificacao, DataCadastro, UsuarioID
                            FROM Novidade
                            WHERE lower(Titulo) like @palavraChave
                            ORDER BY DataCadastro, Titulo
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Novidade
                            WHERE lower(Titulo) like @palavraChave ";

            List<Novidade> novidades = new List<Novidade>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                novidades = con.Query<Novidade>(SQL, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return novidades;
        }
    }
}
