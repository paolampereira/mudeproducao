﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using FisiovidaSystem.Lib.Model;

namespace FisiovidaSystem.Lib.ADO
{
    public class ModalidadeADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private Modalidade Modalidade;
        private List<Modalidade> modalidades;

        public List<ModalidadeTratamentoModel> ListarModalidadesSite()
        {
            List<ModalidadeTratamentoModel> lista = new List<ModalidadeTratamentoModel>();
            string SQL = @"SELECT *
                            FROM Modalidade;";
            string SQL2 = @"SELECT *
                            FROM Tratamento; ";

            modalidades = new List<Modalidade>();
            List<Tratamento> tratamentos = new List<Tratamento>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                modalidades = con.Query<Modalidade>(SQL).ToList();
                tratamentos = con.Query<Tratamento>(SQL2).ToList();
                con.Close();
            }

            foreach (var m in modalidades)
            {
                lista.Add(new ModalidadeTratamentoModel()
                {
                    ID = m.ID,
                    Nome = m.Nome,
                    VagasPorTurma = m.VagasPorTurma,
                    FotoCapa = m.FotoCapa,
                    Duracao = m.Duracao,
                    Tipo = 1
                });
            }
            foreach (var m in tratamentos)
            {
                lista.Add(new ModalidadeTratamentoModel()
                {
                    ID = m.ID,
                    Nome = m.Nome,
                    VagasPorTurma = 1,
                    FotoCapa = m.FotoCapa,
                    Duracao = m.Duracao,
                    Tipo = 2
                });
            }
            lista.OrderBy(l => l.Nome);
            return lista;
        }


        public int TotalModalidadesTratamentos()
        {
            int total = 0;
            string SQL = @"SELECT COUNT(*)
                           FROM Modalidade;";

            string SQL2 = @"SELECT COUNT(*)
                           FROM Tratamento;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                total = con.Query<int>(SQL).FirstOrDefault();
                total += con.Query<int>(SQL2).FirstOrDefault();
                con.Close();
            }
            return total;
        }

        public Modalidade RegistrarImagens(List<string> arquivos, int modalidadeid)
        {
            string SQL = "";
            foreach (var arq in arquivos)
            {
                SQL += @"INSERT INTO ImagemModalidade(
                                    arquivo, modalidadeid)
                            VALUES ('" + arq + "', " + modalidadeid + ");";
            }

            string SELECT = @"SELECT Arquivo, ModalidadeID, ID FROM ImagemModalidade WHERE ModalidadeID = @id;";

            Modalidade = new Modalidade() { ID = modalidadeid };
            Modalidade.Galeria = new List<Imagem>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL);
                Modalidade.Galeria = con.Query<Imagem>(SELECT, new { id = modalidadeid }).ToList();
                con.Close();
            }
            return Modalidade;
        }


        public Modalidade CarregarComValores(int id)
        {
            string SQL = @"SELECT 
                            Modalidade.ID, Modalidade.Nome, Modalidade.Descricao, Modalidade.VagasPorTurma, Modalidade.FotoCapa,
                            Modalidade.Duracao, Modalidade.Inativo, 
                            ValorModalidade.ID, ValorModalidade.Descricao, ValorModalidade.Valor, ValorModalidade.QtdDias,
                            Icone.ID, Icone.Nome, Icone.Imagem
                            FROM Modalidade
                            LEFT JOIN ValorModalidade ON ValorModalidade.ModalidadeID = Modalidade.ID
                            LEFT JOIN IconeModalidade ON IconeModalidade.ModalidadeID = Modalidade.ID
                            LEFT JOIN Icone ON Icone.ID = IconeModalidade.IconeID
                            WHERE Modalidade.ID = @id";
            string SQLImagens = @"SELECT Arquivo, ModalidadeID FROM ImagemModalidade WHERE ModalidadeID = @id;";
            Modalidade = new Modalidade();
            Modalidade.ValoresModalidade = new List<ValorModalidade>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Modalidade, ValorModalidade, Icone, int>(SQL, addResultModalidade, new { id = id });
                Modalidade.Galeria = con.Query<Imagem>(SQLImagens, new { id = id }).ToList();
                con.Close();
            }
            return Modalidade;
        }

        public void Inserir(Modalidade modalidade)
        {
            string SQL = @"BEGIN;
                        INSERT INTO Modalidade(nome, descricao, vagasporturma, duracao, inativo, fotocapa)
                        VALUES (@nome, @descricao, @vagasporturma, @duracao, @inativo, @FotoCapa);";

            foreach (var i in modalidade.TagsIds)
            {
                SQL += @"INSERT INTO IconeModalidade(ModalidadeID, IconeID) VALUES ((SELECT CURRVAL('modalidade_id_seq')), " + i + ");";
            }
            SQL += "COMMIT;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, modalidade);
                con.Close();
            }

        }

        public void Atualizar(Modalidade modalidade)
        {
            string SQL = @"UPDATE modalidade
                           SET nome=@nome, descricao=@descricao, vagasporturma=@vagasporturma
                                , duracao=@duracao, inativo=@inativo, fotocapa = @FotoCapa
                         WHERE ID = @id;";
            string SQLTags = @"SELECT *
                                FROM Icone 
                                INNER JOIN IconeModalidade ON IconeModalidade.IconeID = Icone.ID
                                WHERE ModalidadeID = " + modalidade.ID;
            List<Icone> tagsAntigas = new List<Icone>();
            List<int> Excluir = new List<int>();
            List<int> Inserir = new List<int>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, modalidade);
                tagsAntigas = con.Query<Icone>(SQLTags).ToList();
                con.Close();
            }
            Excluir = tagsAntigas.Where(t => !modalidade.TagsIds.Any(b => b == t.ID)).Select(a => a.ID).ToList();
            Inserir = modalidade.TagsIds.Where(t => !tagsAntigas.Any(b => b.ID == t)).ToList();
            string SQLExcluir = "";
            if (Excluir.Count > 0)
            {
                SQLExcluir = @"DELETE FROM IconeModalidade WHERE IconeID IN (" + String.Join(",", Excluir) + ");";
            }

            string SQLInserir = "";
            foreach (var tag in Inserir)
            {
                SQLInserir += @" INSERT INTO IconeModalidade (IconeID, ModalidadeID)
                            VALUES (" + tag + ", " + modalidade.ID + "); ";
            }

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                if (Excluir.Count > 0)
                {
                    con.Execute(SQLExcluir);
                }
                if (Inserir.Count > 0)
                {
                    con.Execute(SQLInserir);
                }
                con.Close();
            }


        }

        public bool Excluir(int id)
        {
            string SQLTurmas = @"SELECT ID
                            FROM Turma
                            WHERE ModalidadeID = @id;";

            /* TODO
             * string SQLPlanos = @"SELECT COUNT(*)
                            FROM Plano
                            INNER JOIN ValorModalidade vm ON vm.ID = Plano.ValorModalidadeID
                            INNER JOIN Modalidade m ON m.ID = vm.ModalidadeID
                            WHERE vm.ModalidadeID = @id;";
            */
            string SQLValoresIds = @"SELECT vm.ID FROM ValorModalidade vm 
                                    INNER JOIN Modalidade m ON m.ID = vm.ModalidadeID
                                    WHERE m.ID = @id;";

            Modalidade = new Modalidade();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                List<int> turmasIds = con.Query<int>(SQLTurmas, new { id = id }).ToList();

                if (turmasIds.Count > 0)
                {
                    con.Close();
                    return false;
                }
                List<int> valoresIds = con.Query<int>(SQLValoresIds, new { id = id }).ToList();

                string SQLDelete = @"DELETE FROM IconeModalidade WHERE ModalidadeID = @id;
                        DELETE FROM ImagemModalidade WHERE ModalidadeID = @id;";
                if (turmasIds.Count > 0)
                {
                    SQLDelete += @"DELETE FROM PlanoTurma WHERE TurmaID IN (" + string.Join(",", turmasIds) + @");";
                }

                if (valoresIds.Count > 0)
                {
                    SQLDelete += @"UPDATE Plano
                           SET ValorModalidadeID = null WHERE ValorModalidadeID IN (" + string.Join(",", valoresIds) + @");
                        UPDATE PlanoLog
                           SET ValorModalidadeID = null WHERE ValorModalidadeID IN (" + string.Join(",", valoresIds) + @");
                        DELETE FROM ValorModalidade WHERE ID IN (" + string.Join(",", valoresIds) + @");";
                }
                SQLDelete += @"DELETE FROM Modalidade WHERE ID = @id;";

                con.Execute(SQLDelete, new { id = id });
                con.Close();
            }

            return true;
        }

        public Modalidade Carregar(int id)
        {
            string SQL = @"SELECT 
                            Modalidade.ID, Modalidade.Nome, Modalidade.Descricao, Modalidade.VagasPorTurma, Modalidade.FotoCapa,
                            Modalidade.Duracao, Modalidade.Inativo
                            FROM Modalidade
                            WHERE Modalidade.ID = @id";
            string SQLImagens = @"SELECT ID, Arquivo, ModalidadeID FROM ImagemModalidade WHERE ModalidadeID = @id;";

            string SQLIcones = @"SELECT Icone.ID, Icone.Imagem, Icone.Nome
                                FROM Icone 
                                INNER JOIN IconeModalidade ON IconeModalidade.IconeID = Icone.ID
                                WHERE ModalidadeID = @id;";
            Modalidade = new Modalidade();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                Modalidade = con.Query<Modalidade>(SQL, new { id = id }).FirstOrDefault();
                Modalidade.Galeria = con.Query<Imagem>(SQLImagens, new { id = id }).ToList();
                Modalidade.Icones = con.Query<Icone>(SQLIcones, new { id = id }).ToList();
                con.Close();
            }
            return Modalidade;
        }
        public List<Modalidade> ListarComPlanos()
        {
            string SQL = @"SELECT 
                            Modalidade.ID, Modalidade.Nome, Modalidade.Descricao, Modalidade.VagasPorTurma, 
                            Modalidade.Duracao, Modalidade.Inativo,
                            ValorModalidade.ID, ValorModalidade.Descricao, ValorModalidade.Valor, ValorModalidade.QtdDias
                            FROM Modalidade
                            INNER JOIN ValorModalidade ON ValorModalidade.ModalidadeID = Modalidade.ID 
                            ORDER BY ValorModalidade.QtdDias ";
            Modalidade = new Modalidade();
            modalidades = new List<Modalidade>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Modalidade, ValorModalidade, int>(SQL, addResultModalidades);
                con.Close();
            }
            return modalidades;
        }
        public int addResultModalidades(Modalidade modalidade, ValorModalidade valorModalidade)
        {
            Modalidade m = modalidades.FirstOrDefault(p => p.ID == modalidade.ID);
            if (m != null)
            {
                m.ValoresModalidade.Add(valorModalidade);
            }
            else
            {
                modalidade.ValoresModalidade = new List<ValorModalidade>();
                modalidade.ValoresModalidade.Add(valorModalidade);
                modalidades.Add(modalidade);
            }

            return 1;
        }
        public int addResultModalidade(Modalidade modalidade, ValorModalidade valorModalidade, Icone icone)
        {

            if (Modalidade.ID <= 0)
            {
                Modalidade = modalidade;
                Modalidade.Icones = new List<Icone>();
                Modalidade.ValoresModalidade = new List<ValorModalidade>();
                if (valorModalidade != null)
                {
                    Modalidade.ValoresModalidade.Add(valorModalidade);
                }
                if (icone != null)
                {
                    Modalidade.Icones.Add(icone);
                }
            }
            else
            {
                if (icone != null)
                {
                    if (!Modalidade.Icones.Any(p => p.ID == icone.ID))
                    {
                        Modalidade.Icones.Add(icone);
                    }
                }
                if (valorModalidade != null)
                {
                    if (!Modalidade.ValoresModalidade.Any(p => p.ID == valorModalidade.ID))
                    {
                        Modalidade.ValoresModalidade.Add(valorModalidade);
                    }
                }
            }

            return 1;
        }
        public List<Modalidade> ListarIdsNomes()
        {
            string SQL = @"SELECT 
                            Modalidade.ID, Modalidade.Nome
                           FROM Modalidade ORDER BY Nome ";

            modalidades = new List<Modalidade>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                modalidades = con.Query<Modalidade>(SQL).ToList();
                con.Close();
            }
            return modalidades;
        }
        public List<Modalidade> Listar(int skip, int take, string palavraChave)
        {
            string SQL = @"SELECT 
                            Modalidade.ID, Modalidade.Nome, Modalidade.Descricao, Modalidade.VagasPorTurma, 
                            Modalidade.Duracao, Modalidade.Inativo 
                            FROM Modalidade
                            WHERE lower(Nome) like @palavraChave 
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                            FROM Modalidade
                            WHERE lower(Nome) like @palavraChave ";
            modalidades = new List<Modalidade>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                modalidades = con.Query<Modalidade>(SQL, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave.ToLower() + "%",
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return modalidades;
        }

    }
}
