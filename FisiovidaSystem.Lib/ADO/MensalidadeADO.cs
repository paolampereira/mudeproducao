﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using Dapper;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Lib.ADO
{
    public class MensalidadeADO : ADOSuper
    {
        public int TotalRegistros { get; set; }
        private Mensalidade Mensalidade;
        private List<Mensalidade> Mensalidades;
/*
        public List<Mensalidade> ListarMensalidades(int alunoid)
        {
            Mensalidades = new List<Mensalidade>();
            string SQL = @"SELECT  p.ID, p.AlunoID, p.ProfessorID, p.DataInicio,
                            p.ValorModalidadeID, p.SaldoAulas, 
                            p.DataFim, p.TratamentoID,
                            v.ID, v.Valor, v.QtdDias
                            FROM Plano p 
                            INNER JOIN ValorModalidade v ON v.ID = p.ValorModalidadeID
                            WHERE p.AlunoID = @AlunoID AND (p.DataFim IS NULL OR 
                            (extract(month from p.DataFim) =  extract(month from now()) AND 
                            extract(year from p.DataFim) =  extract(year from now()) ));";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Plano, ValorModalidade, int>(SQL, addResultMensalidade, new { AlunoID = alunoid });
                con.Close();
            }
            return Mensalidades;
        }*/
        public List<Mensalidade> ListarMensalidades(int alunoid)
        {
            Mensalidades = new List<Mensalidade>();
            string SQL = @"SELECT  m.ID, m.AlunoID, m.DataPagamento, m.Valor, m.ValorPago, m.Referente,
                            p.ID, p.DataInicio
                            FROM Mensalidade m 
                            INNER JOIN PlanoMensalidade pm ON pm.MensalidadeID = m.ID
                            INNER JOIN Plano p ON p.ID = pm.PlanoID
                            WHERE m.AlunoID = @AlunoID;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Query<Mensalidade, Plano, int>(SQL, addResultMensalidade,new { AlunoID = alunoid });
                con.Close();
            }
            return Mensalidades;
        }
        public int addResultMensalidade(Mensalidade m, Plano p)
        {
            if (m != null && p.ID > 0)
            {
                if(!Mensalidades.Any(a => a.ID == m.ID))
                {
                    m.PlanosIds = new List<int>();
                    m.PlanosIds.Add(p.ID);
                    Mensalidades.Add(m);
                }
                else
                {
                    Mensalidade mensalidade = Mensalidades.FirstOrDefault(a => a.ID == m.ID);
                    mensalidade.PlanosIds.Add(p.ID);
                }
            }
            return 1;
        }
        
        public void RegistrarPagamento(Mensalidade mensalidade)
        {
            string SQL = @"BEGIN;
                            INSERT INTO Mensalidade (alunoid, datapagamento, valor, valorpago, referente)
                            VALUES (@AlunoID, @DataPagamento, @Valor, @ValorPago, @Referente);";

            mensalidade.DataPagamento = DateTime.Now;

            foreach (var p in mensalidade.PlanosIds)
            {
                SQL += @"INSERT INTO PlanoMensalidade (mensalidadeid, planoid) 
                            VALUES ((SELECT CURRVAL('mensalidade_id_seq')), " + p + ");";
            }
            foreach (var d in mensalidade.Descontos)
            {
                SQL += @"INSERT INTO DescontoMensalidade (mensalidadeid, descontoid) 
                            VALUES ((SELECT CURRVAL('mensalidade_id_seq')), " + d.ID + ");";
            }
            SQL += @"COMMIT;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, mensalidade);
                con.Close();
            }
        }

    }
}
