﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Npgsql;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Lib.ADO
{
    public class DescontoADO : ADOSuper
    {
        public int TotalRegistros { get; set; }

        public Desconto Carregar(int id)
        {
            string SQL = @"SELECT 
                            ID, Descricao, Porcentagem, Inativo
                            FROM Desconto
                            WHERE ID = @id";

            Desconto desconto = new Desconto();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                desconto = con.Query<Desconto>(SQL, new
                {
                    id = id
                }).FirstOrDefault();

                con.Close();
            }
            return desconto;
        }

        public List<Desconto> Listar(int skip, int take, string palavraChave)
        {
            string SQL = @"SELECT 
                            ID, Descricao, Porcentagem, Inativo
                            FROM Desconto
                            WHERE Descricao like @palavraChave OR Porcentagem = @porc 
                            ORDER BY Porcentagem
                          OFFSET @skip LIMIT @take";

            string SQLCount = @"SELECT COUNT(*)
                                FROM Desconto
                                WHERE Descricao like @palavraChave OR Porcentagem = @porc ";

            List<Desconto> descontos = new List<Desconto>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();

                int porc = 0;
                if (Int32.TryParse(palavraChave, out porc))
                {
                    porc = Int32.Parse(palavraChave);
                }

                descontos = con.Query<Desconto>(SQL, new
                {
                    palavraChave = "%" + palavraChave + "%",
                    porc = porc,
                    skip = skip,
                    take = take
                }).ToList();

                TotalRegistros = con.Query<int>(SQLCount, new
                {
                    palavraChave = "%" + palavraChave + "%",
                    porc = porc,
                    skip = skip,
                    take = take
                }).FirstOrDefault();


                con.Close();
            }
            return descontos;
        }

        public List<Desconto> ListarAtivos()
        {
            string SQL = @"SELECT 
                            ID, Descricao, Porcentagem, Inativo
                            FROM Desconto
                            WHERE Inativo = false
                            ORDER BY Porcentagem";

            List<Desconto> descontos = new List<Desconto>();

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                descontos = con.Query<Desconto>(SQL).ToList();
                con.Close();
            }
            return descontos;
        }

        public void Cadastrar(Desconto desconto)
        {
            string SQL = @"INSERT INTO Desconto
                            (id, porcentagem, descricao, inativo)
                        VALUES (@id, @porcentagem, @descricao, @inativo)";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, desconto);
                con.Close();
            }
        }

        public void AlterarInativo(Desconto desconto)
        {
            string SQL = @"UPDATE public.desconto
                               SET inativo=@inativo
                             WHERE id = @id";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, desconto);
                con.Close();
            }
        }

        public void Alterar(Desconto desconto)
        {
            string SQL = @"UPDATE public.desconto
                               SET porcentagem=@porcentagem, descricao=@descricao, inativo=@inativo
                             WHERE id = @id";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, desconto);
                con.Close();
            }
        }

        public List<Desconto> CarregarDescontos(List<int> ids)
        {
            List<Desconto> descontos = new List<Desconto>();
            string list = String.Join(",", ids);

            string SQL = @"SELECT d.ID, d.Porcentagem, d.Descricao, d.Inativo
                            FROM Desconto d 
                            WHERE d.ID IN (" + String.Join(",", list) + ")";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                descontos = con.Query<Desconto>(SQL).ToList();
                con.Close();
            }
            return descontos;

        }
    }
}
