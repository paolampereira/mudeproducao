﻿using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Npgsql;

namespace FisiovidaSystem.Lib.ADO
{
    public class InscricaoTempADO : ADOSuper
    {
        public void DesmarcarAula(InscricaoTemp inscricao)
        {
            string SQL = @"BEGIN;
                            UPDATE Plano
                               SET saldoaulas = (saldoaulas + 1)
                             WHERE ID = @PlanoID;
                            INSERT INTO InscricaoTemp(turmaid, alunoid, desmarcar, dia, planoid)
                            VALUES (@TurmaID, @AlunoID, @Desmarcar, @Dia, @PlanoID);
                           COMMIT;";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, inscricao);
                con.Close();
            }
        }
        public List<InscricaoTemp> ListarCancelamentosAluno(int AlunoID)
        {
            string SQL = @"SELECT ID, TurmaID, AlunoID, Desmarcar, Dia, PlanoID
                            FROM InscricaoTemp
                            WHERE AlunoID = @id AND Dia >= current_date AND Desmarcar = true ";
            List<InscricaoTemp> retorno = new List<InscricaoTemp>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                retorno = con.Query<InscricaoTemp>(SQL, new { id = AlunoID }).ToList();
                con.Close();
            }
            return retorno;
        }


        public List<InscricaoTemp> ListarCancelamentosComDetalhesAluno(int AlunoID)
        {
            string SQL = @"SELECT 
                                i.ID, i.TurmaID, i.AlunoID, i.Desmarcar, i.Dia, i.PlanoID,
                                Horario.ID, Horario.Hora, Horario.DiaSemana,
                                Modalidade.ID, Modalidade.Nome,
                                Usuario.ID, Usuario.Nome
                            FROM InscricaoTemp i
                            INNER JOIN Turma ON Turma.ID = i.TurmaID
                            INNER JOIN Horario ON Horario.ID = Turma.HorarioID
                            INNER JOIN Usuario ON Usuario.ID = Turma.ProfessorID
                            INNER JOIN Modalidade ON Modalidade.ID = Turma.ModalidadeID
                            WHERE AlunoID = @id AND Dia >= current_date AND Desmarcar = true ";
            List<InscricaoTemp> retorno = new List<InscricaoTemp>();
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                retorno = con.Query<InscricaoTemp, Horario, Modalidade, Usuario, InscricaoTemp>(SQL, addResult, new
                {
                    id = AlunoID
                }).ToList();
                con.Close();
            }
            return retorno;
        }
        public InscricaoTemp addResult(InscricaoTemp it, Horario horario, Modalidade modalidade, Usuario professor)
        {
            if (it != null)
            {
                it.Horario = horario;
                it.Professor = professor;
                it.Modalidade = modalidade;
            }
            return it;
        }
    }
}
