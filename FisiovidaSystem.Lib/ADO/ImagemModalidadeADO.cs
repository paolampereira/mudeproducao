﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Npgsql;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Lib.ADO
{
    public class ImagemModalidadeADO : ADOSuper
    {

        public Imagem Excluir(int id)
        {
            Imagem retorno = new Imagem();
            string SQLNome = @"SELECT * FROM ImagemModalidade
                            WHERE id = @id";

            string SQL = @"DELETE FROM ImagemModalidade
                            WHERE id = @id";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                retorno = con.Query<Imagem>(SQLNome, new { id = id }).FirstOrDefault();
                con.Execute(SQL, new { id = id });
                con.Close();
            }
            return retorno;
        }
    }
}
