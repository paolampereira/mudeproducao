﻿using FisiovidaSystem.Lib.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace FisiovidaSystem.Lib.ADO
{
    public class ConfiguracaoADO : ADOSuper
    {
        public int TotalRegistros { get; set; }

        public Configuracao CarregarVariaveisSite()
        {
            Configuracao config = new Configuracao();

            string SQL = @"SELECT BannerPropaganda, LinkPropaganda
                            FROM Configuracao";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                config = con.Query<Configuracao>(SQL).FirstOrDefault();
                con.Close();
            }
            return config;
        }


        public Configuracao CarregarVariaveisContato()
        {
            Configuracao config = new Configuracao();

            string SQL = @"SELECT BannerPropaganda, LinkPropaganda, Email, Endereco, Telefone, Latitude, Longitude
                            FROM Configuracao";

            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                config = con.Query<Configuracao>(SQL).FirstOrDefault();
                con.Close();
            }
            return config;
        }


        public void Salvar(Configuracao config)
        {
            string SQL = @"UPDATE Configuracao
                   SET periodo = @periodo, 
                        minimohoras=@minimohoras, numerosessoes = @numerosessoes,
                        email=@email, telefone=@telefone, endereco=@endereco,
                        latitude=@latitude, longitude= @longitude, BannerPropaganda = @BannerPropaganda,
                        LinkPropaganda = @LinkPropaganda;";
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                con.Execute(SQL, config);
                con.Close();
            }
        }

        public Configuracao Carregar()
        {
            Configuracao config = new Configuracao();

            string SQL = @"SELECT *
                            FROM Configuracao";
            
            using (NpgsqlConnection con = this.CreateConnection())
            {
                con.Open();
                config = con.Query<Configuracao>(SQL).FirstOrDefault();
                con.Close();
            }
            return config;
        }
    }
}
