﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Modalidade
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int VagasPorTurma { get; set; }
        public int Duracao { get; set; }
        public bool Inativo { get; set; }
        public string FotoCapa { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int w { get; set; }
        public int h { get; set; }
        public List<int> TagsIds { get; set; }
        public List<Imagem> Galeria { get; set; }
        public List<ValorModalidade> ValoresModalidade { get; set; }
        public List<Icone> Icones { get; set; }
        public List<Icone> IconesSistema { get; set; }
    }
}
