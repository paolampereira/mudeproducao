﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Aluno
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Foto { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public DateTime DataNascimento { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
        public char Sexo { get; set; }
        public bool Inativo { get; set; }
        public string Telefone { get; set; }
        public string Telefone2 { get; set; }
        public bool EmDia { get; set; }
        public string CancelamentosTurma(int turmaid)
        {
            var stringDesmarcacoes = "";

            var desmarcacoes = Cancelamentos.Where(a => a.TurmaID == turmaid);
            foreach (var d in desmarcacoes)
            {
                stringDesmarcacoes += "\'" + d.Dia.ToString("dd/MM/yyyy") + "\',";
            }
            if (stringDesmarcacoes.Length > 1)
            {
                int n = stringDesmarcacoes.Length - 1;
                stringDesmarcacoes = stringDesmarcacoes.Substring(0, n);
            }
            return stringDesmarcacoes;
        }
        public string TelefonesFormatados
        {
            get
            {
                string tels = String.Format("({0}) {1}-{2}", Telefone.Substring(0, 2), Telefone.Substring(2, 4), Telefone.Substring(6));
                if (!String.IsNullOrEmpty(Telefone2))
                {
                    tels += String.Format(" ({0}) {1}-{2} ", Telefone2.Substring(0, 2), Telefone2.Substring(2, 4), Telefone2.Substring(6));
                }
                return tels;
            }
        }

        public List<Plano> Planos { get; set; }
        public List<Plano> PlanosAntigos { get; set; }
        public List<InscricaoTemp> Recuperacoes { get; set; }
        public List<InscricaoTemp> Cancelamentos { get; set; }
        public List<Mensalidade> MensalidadesPagas { get; set; }
        public List<Mensalidade> MensalidadesAtrasadas { get; set; }

    }
}
