﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class AlunoTurma
    {
        public int TurmaID { get; set; }
        public int AlunoID { get; set; }
    }
}
