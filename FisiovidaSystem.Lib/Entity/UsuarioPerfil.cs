﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class UsuarioPerfil
    {
        public int ID { get; set; }
        public string Nome { get; set; }
    }
}
