﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Novidade
    {
        public int ID { get; set; }
        public string Texto { get; set; }
        public string Titulo { get; set; }
        public string Imagem { get; set; }
        public string ImagemCapa { get; set; }
        public string ImagemDestaque { get; set; }
        public bool Destaque { get; set; }
        public int UsuarioID { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int w { get; set; }
        public int h { get; set; }
        public int x1 { get; set; }
        public int y1 { get; set; }
        public int w1 { get; set; }
        public int h1 { get; set; }
        public string Chamada { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
        public List<Tag> Tags { get; set; }
        public List<int> TagsIds { get; set; }
        public List<Tag> TagsSistema { get; set; }
        public Usuario Usuario { get; set; }
        public List<Imagem> Galeria { get; set; }
        public bool EditouCrop { get; set; }
        public bool EditouCapa { get; set; }
    }
}
