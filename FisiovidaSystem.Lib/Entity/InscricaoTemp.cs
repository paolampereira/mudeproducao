﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class InscricaoTemp
    {
        public int ID { get; set; }
        public int TurmaID { get; set; }
        public int AlunoID { get; set; }
        public bool Desmarcar { get; set; }
        public int PlanoID { get; set; }
        public DateTime Dia { get; set; }

        public Usuario Professor { get; set; }
        public Modalidade Modalidade { get; set; }
        public Horario Horario { get; set; }
    }
}
