﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Horario
    {
        public int ID { get; set; }
        public int DiaSemana { get; set; }
        public DateTime Hora { get; set; }
        public bool Inativo { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataModificacao { get; set; }
        public int UsuarioID { get; set; }
        public Usuario Usuario { get; set; }
        public string NomeDiaSemana
        {
            get
            {

                string retorno = "";
                switch (DiaSemana)
                {
                    case 0:
                        retorno = "Dom";
                        break;
                    case 1:
                        retorno = "Seg";
                        break;
                    case 2:
                        retorno = "Ter";
                        break;
                    case 3:
                        retorno = "Qua";
                        break;
                    case 4:
                        retorno = "Qui";
                        break;
                    case 5:
                        retorno = "Sex";
                        break;
                    case 6:
                        retorno = "Sab";
                        break;
                }
                return retorno;
            }
        }
        public string RestanteSemana
        {
            get
            {

                string retorno = "";
                List<int> semana = new List<int>() { 0, 1, 2, 3, 4, 5, 6 };
                semana.Remove(this.DiaSemana);
                retorno = String.Join(",", semana.ToArray());
                return retorno;
            }
        }
        public string DefaultDate
        {
            get{
                DateTime hoje = DateTime.Now;
                int daysToAdd = (this.DiaSemana - (int)hoje.DayOfWeek + 7) % 7;
                string dia = hoje.AddDays(daysToAdd).ToString("dd/MM/yyyy");
                return dia;
            }
            
        }
    }
}
