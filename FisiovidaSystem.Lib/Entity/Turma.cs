﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Turma
    {
        public int ID { get; set; }
        public int ModalidadeID { get; set; }
        public int TratamentoID { get; set; }
        public int HorarioID { get; set; }
        public int ProfessorID { get; set; }
        public bool Inativo { get; set; }
        public int VagasDisponiveis
        {
            get
            {
                if (Modalidade != null)
                {
                    return Modalidade.VagasPorTurma - VagasOcupadas;
                }
                return 1 - VagasOcupadas;
            }
            
        }
        public int VagasOcupadas { get; set; }

        public int PlanoID { get; set; }
        public Usuario Professor { get; set; }
        public Horario Horario { get; set; }
        public Modalidade Modalidade { get; set; }
        public Tratamento Tratamento { get; set; }
        public List<Aluno> Alunos { get; set; }
    }
}
