﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Imagem
    {
        public int ID { get; set; }
        public int ModalidadeID { get; set; }
        public int NovidadeID { get; set; }
        public string Arquivo { get; set; }
    }
}
