﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Usuario
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Foto { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public bool Master { get; set; }
        public bool Inativo { get; set; }
        public bool Professor { get; set; }
        public char Sexo { get; set; }
        public string DescricaoProfissional { get; set; }

        public int x { get; set; }
        public int y { get; set; }
        public int w { get; set; }
        public int h { get; set; }

        public string ConfirmarSenha { get; set; }
        public List<Turma> Turmas { get; set; }
    }
}
