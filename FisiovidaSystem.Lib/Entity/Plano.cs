﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Plano
    {
        public int ID { get; set; }
        public int AlunoID { get; set; }
        public int ProfessorID { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int ValorModalidadeID { get; set; }
        public int TratamentoID { get; set; }
        public int SaldoAulas { get; set; }

        public Usuario Professor { get; set; }
        public ValorModalidade ValorModalidade { get; set; }
        public List<Turma> TurmasPlano { get; set; }
        public List<int> TurmasIDs { get; set; }

    }
}
