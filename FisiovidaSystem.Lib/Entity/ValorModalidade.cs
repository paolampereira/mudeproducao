﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class ValorModalidade
    {
        public int ID { get; set; }
        public int ModalidadeID { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }
        public int QtdDias { get; set; }
        public Modalidade Modalidade { get; set; }
    }
}
