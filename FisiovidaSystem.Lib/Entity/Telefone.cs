﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Telefone
    {
        public int ID { get; set; }
        public int AlunoID { get; set; }
        public string Numero { get; set; }
        public int UsuarioID { get; set; }
    }
}
