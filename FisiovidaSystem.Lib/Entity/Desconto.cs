﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Desconto
    {
        public int ID { get; set; }
        public double Porcentagem { get; set; }
        public string Descricao { get; set; }
        public bool Inativo { get; set; }
    }
}
