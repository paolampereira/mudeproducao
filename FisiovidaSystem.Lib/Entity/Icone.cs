﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Icone
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Imagem { get; set; }
    }
}
