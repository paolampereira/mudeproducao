﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Mensalidade
    {
        public int ID { get; set; }
        public int AlunoID { get; set; }
        public DateTime DataPagamento { get; set; }
        public DateTime Referente { get; set; }
        public double Valor { get; set; }
        public double ValorPago { get; set; }
        public List<Desconto> Descontos { get; set; }
        public int PlanoID { get; set; }
        public List<Plano> Planos { get; set; }
        public List<int> PlanosIds { get; set; }
    }
}
