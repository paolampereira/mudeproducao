﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Entity
{
    public class Tag
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Cor { get; set; }
        public string Descricao { get; set; }
        public bool Diferencial { get; set; }
    }
}
