﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class NovidadeService
    {
        private NovidadeADO dal = new NovidadeADO();
        public int TotalRegistros { get; set; }

        public List<Novidade> Listar(int pagina = 1)
        {
            List<Novidade> lista = dal.Listar(6, (pagina - 1) * 6);
            this.TotalRegistros = dal.TotalRegistros;
            return lista;
        }

        public void RemoverImagem(int id)
        {
            dal.RemoverImagem(id);
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        public List<Novidade> ListarDestaques()
        {
            return dal.ListarDestaques();
        }

        public RetornoModel Salvar(Novidade novidade)
        {
            novidade.DataModificacao = DateTime.Now;

            RetornoModel retorno = ValidaNovidade(novidade);
            if (!retorno.Sucesso)
            {
                return retorno;
            }

            if (novidade.ID > 0)
            {
                dal.Alterar(novidade);
            }
            else
            {
                novidade.DataCadastro = DateTime.Now;
                dal.Cadastrar(novidade);
            }
            return new RetornoModel() { Sucesso = true, Mensagem = "Dados salvos com sucesso!" };
        }
        public Novidade RegistrarImagens(List<string> imagens, int novidadeID)
        {
            dal.RegistrarImagens(imagens, novidadeID);
            return dal.Carregar(novidadeID);
        }

        private RetornoModel ValidaNovidade(Novidade novidade)
        {
            if (String.IsNullOrEmpty(novidade.Titulo) || String.IsNullOrEmpty(novidade.Texto) || novidade.TagsIds.Count == 0)
            {
                return new RetornoModel() { Mensagem = "Campo(s) vazio(s)", Sucesso = false };
            }

            return new RetornoModel() { Mensagem = "ok", Sucesso = true };
        }

        public List<Novidade> Listar(int skip, int take, string palavraChave)
        {
            return dal.Listar(skip, take, palavraChave);
        }

        public Novidade Carregar(int id)
        {
            return dal.Carregar(id);
        }
    }
}
