﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class DescontoService
    {
        public int TotalRegistros { get; set; }
        private DescontoADO dal = new DescontoADO();
        public List<Desconto> CarregarDescontos(List<int> ids)
        {
            return dal.CarregarDescontos(ids);
        }

        public Desconto CarregarDados(int id)
        {
            return dal.Carregar(id);
        }


        public void Salvar(Desconto desconto)
        {
            desconto.Porcentagem /= 100;
            if(desconto.ID > 0)
            {
                dal.Alterar(desconto);
            }
            else
            {
                dal.Cadastrar(desconto);
            }
        }

        public void AlterarInativo(Desconto desconto)
        {
            dal.AlterarInativo(desconto);
        }

        public List<Desconto> ListarAtivos()
        {
            return dal.ListarAtivos();
        }

        public List<Desconto> Listar(int skip, int take, string palavraChave)
        {
            return dal.Listar(skip, take, palavraChave);
        }

    }
}
