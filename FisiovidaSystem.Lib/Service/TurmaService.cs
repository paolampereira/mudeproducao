﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class TurmaService
    {
        TurmaADO dal = new TurmaADO();
        public int TotalRegistros { get; set; }

        public List<Turma> Listar(int skip, int take, int diaSemana, int professorID)
        {
            List<Turma> retorno = dal.Listar(skip, take,  diaSemana, professorID);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public bool Excluir(int id)
        {
            return dal.Excluir(id);
        }

        public Turma Carregar(int id)
        {
            return dal.Carregar(id);
        }
        public void Salvar(Turma turma)
        {
            if (turma.ID > 0)
            {

            }
            else
            {
                dal.Cadastrar(turma);
            }
        }
    }
}
