﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class IconeService
    {
        private IconeADO dal = new IconeADO();
        public int TotalRegistros { get; set; }

        public List<Icone> Listar(int skip, int take, string palavraChave)
        {
            List<Icone> retorno = dal.Listar(skip, take, palavraChave);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        public List<Icone> Listar()
        {
            List<Icone> retorno = dal.Listar();
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public Icone Carregar(int id)
        {
            return dal.Carregar(id);
        }

        public void Salvar(Icone icone)
        {
            if (string.IsNullOrEmpty(icone.Nome))
            {
                throw new Exception("Nome vazio");
            }

            if (icone.ID <= 0)
            {
                dal.Cadastrar(icone);
            }
            else
            {
                dal.Alterar(icone);
            }
        }
    }
}
