﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class AlunoService
    {
        private AlunoADO dal = new AlunoADO();
        public int TotalRegistros { get; set; }

        public void SairTurma(int usuarioid, int turmaid, int planoid)
        {
            PlanoService pservice = new PlanoService();
            pservice.SairTurma(planoid, turmaid, usuarioid);
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        public Aluno SaldoDevedor(Aluno aluno)
        {
            //buscando mensalidades pagas com seus planos
            MensalidadeService mservice = new MensalidadeService();
            aluno.MensalidadesPagas = mservice.ListarMensalidadesAluno(aluno.ID);
            aluno.MensalidadesAtrasadas = new List<Mensalidade>();

            DateTime inicioMes = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            aluno.EmDia = false;
            if (aluno.MensalidadesPagas.Any(m => m.Referente.Month == inicioMes.Month && m.Referente.Year == inicioMes.Year))
            {
                aluno.EmDia = true;
            }
            else
            {
                //comparando periodos dos planos com mensalidades para ver quais estão atrasadas
                foreach (var plano in aluno.Planos)
                {
                    List<Mensalidade> mensPlano = aluno.MensalidadesPagas.Where(m => m.PlanosIds.Contains(plano.ID)).ToList();
                    //conta a quantidade de meses de cada plano
                    int qtdMensalidades;
                    DateTime dataFim = DateTime.Now;
                    if (plano.DataFim == DateTime.MinValue)
                    {
                        qtdMensalidades = ((plano.DataInicio.Year - DateTime.Now.Year) * 12) + DateTime.Now.Month - plano.DataInicio.Month;
                    }
                    else
                    {
                        dataFim = plano.DataFim;
                        qtdMensalidades = ((plano.DataInicio.Year - plano.DataFim.Year) * 12) + plano.DataFim.Month - plano.DataInicio.Month;
                    }
                    //conta as mensalidades que contém aquele plano
                    int qtdPagas = mensPlano.Count;

                    if (qtdMensalidades > qtdPagas) //se tem em aberto
                    {
                        DateTime startDate = plano.DataInicio;
                        //percorrendo periodo do plano
                        while (startDate.Month != dataFim.Month || startDate.Year != dataFim.Year)
                        {
                            Mensalidade men = aluno.MensalidadesAtrasadas.FirstOrDefault(m => m.Referente.Month == startDate.Month && m.Referente.Year == startDate.Year);
                            //se tem alguma atrasada esse mes, quer dizer que todos estão
                            if (men != null)
                            {
                                men.PlanosIds.Add(plano.ID);
                                men.Valor += plano.ValorModalidade.Valor;
                            }
                            else if (!mensPlano.Any(m => m.Referente.Month == startDate.Month && m.Referente.Year == startDate.Year))
                            {
                                aluno.MensalidadesAtrasadas.Add(new Mensalidade()
                                {
                                    Referente = startDate,
                                    PlanosIds = new List<int>() { plano.ID },
                                    Valor = plano.ValorModalidade.Valor
                                });
                            }
                            startDate = startDate.AddMonths(1);
                        }
                    }
                }

            }
            //organizando planos

            //Planos encerrados meses anteriores
            aluno.PlanosAntigos = aluno.Planos.Where(p => DateTime.Compare(p.DataFim, inicioMes) <= 0 && p.DataFim != DateTime.MinValue).ToList();
            //Planos ativos até esse mês
            aluno.Planos = aluno.Planos.Where(p => DateTime.Compare(p.DataFim, inicioMes) > 0 || p.DataFim == DateTime.MinValue).ToList();

            return aluno;
        }

        public Aluno Carregar(int id)
        {
            Aluno aluno = dal.Carregar(id);
            aluno = SaldoDevedor(aluno);

            return aluno;
        }

        public Aluno CarregarDetalhes(int id)
        {
            Aluno aluno = new Aluno();
            aluno = Carregar(id);
            aluno.Cancelamentos = ListarCancelamentosComDetalhesAluno(id);
            return aluno;
        }

        public Aluno CarregarDados(int id)
        {
            return dal.CarregarDados(id);
        }

        public Aluno Carregar(string username)
        {
            return dal.Carregar(username);
        }

        public Aluno Logar(string login, string senha)
        {
            Aluno retorno = dal.Carregar(login);
            if (retorno == null)
            {
                return null;
            }
            if (retorno.Senha == senha)
            {
                retorno.Senha = "";
                return retorno;
            }
            return null;
        }

        public List<InscricaoTemp> ListarCancelamentosComDetalhesAluno(int alunoID)
        {
            InscricaoTempService itService = new InscricaoTempService();
            return itService.ListarCancelamentosComDetalhesAluno(alunoID);
        }

        public List<InscricaoTemp> ListarCancelamentosAlunoTurma(int alunoID, int turmaid)
        {
            InscricaoTempService itService = new InscricaoTempService();
            return itService.ListarCancelamentosAluno(alunoID).Where(t => t.TurmaID == turmaid).ToList();
        }

        public RetornoModel Salvar(Aluno aluno)
        {
            aluno.DataNascimento = new DateTime(aluno.DataNascimento.Year, aluno.DataNascimento.Day, aluno.DataNascimento.Month);
            RetornoModel retorno = ValidaAluno(aluno);
            if (retorno.Sucesso)
            {
                aluno.Telefone = aluno.Telefone.Replace("(", "");
                aluno.Telefone = aluno.Telefone.Replace(")", "");
                aluno.Telefone = aluno.Telefone.Replace(" ", "");
                aluno.Telefone = aluno.Telefone.Replace("-", "");
                aluno.Telefone = aluno.Telefone.Replace("_", "");
                if (!String.IsNullOrEmpty(aluno.Telefone2))
                {
                    aluno.Telefone2 = aluno.Telefone2.Replace("_", "");
                    aluno.Telefone2 = aluno.Telefone2.Replace("(", "");
                    aluno.Telefone2 = aluno.Telefone2.Replace(")", "");
                    aluno.Telefone2 = aluno.Telefone2.Replace(" ", "");
                    aluno.Telefone2 = aluno.Telefone2.Replace("-", "");
                }
                aluno.CPF = aluno.CPF.Replace(".", "");
                aluno.CPF = aluno.CPF.Replace("-", "");

                AlunoADO ado = new AlunoADO();
                if (aluno.ID > 0)
                {
                    aluno.DataModificacao = DateTime.Now;
                    ado.Editar(aluno);
                }
                else
                {
                    aluno.Foto = "default_avatar.png";
                    aluno.Senha = aluno.CPF;
                    ado.Cadastrar(aluno);
                }
            }
            return retorno;
        }

        public RetornoModel ValidaAluno(Aluno aluno)
        {
            if (String.IsNullOrEmpty(aluno.Nome))
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Campo Nome Obrigatório" };
            }
            if (aluno.Nome.Count() < 10)
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Nome inválido" };
            }

            if (aluno.DataNascimento > DateTime.Now)
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Data de Nascimento inválida" };
            }
            if (!ValidationUtil.IsValideMail(aluno.Email))
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Email inválido" };
            }
            if (!ValidationUtil.IsValidCPF(aluno.CPF))
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "CPF inválido" };
            }
            if (String.IsNullOrEmpty(aluno.Login))
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Login inválido" };
            }
            if (String.IsNullOrEmpty(aluno.Telefone))
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Telefone inválido" };
            }
            return new RetornoModel() { Sucesso = true, Mensagem = "Ok" };
        }

        public List<Aluno> Listar(int skip, int take, string palavraChave)
        {
            List<Aluno> retorno = dal.Listar(skip, take, palavraChave);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

    }
}
