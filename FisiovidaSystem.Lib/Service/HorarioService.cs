﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class HorarioService
    {
        HorarioADO dal = new HorarioADO();
        public int TotalRegistros { get; set; }

        public bool Excluir(int id)
        {
            return dal.Excluir(id);
        }

        public List<Horario> Listar(int skip, int take)
        {
            List<Horario> retorno = dal.Listar(skip, take);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }
        public Horario Carregar(int id)
        {
            return dal.Carregar(id);
        }

        public RetornoModel Salvar(Horario horario)
        {
            RetornoModel retorno = ValidaHorario(horario);
            if (retorno.Sucesso)
            {
                if (horario.ID > 0)
                {
                    horario.DataModificacao = DateTime.Now;
                    dal.Atualizar(horario);
                }
                else
                {
                    dal.Salvar(horario);
                }   
            }
            return retorno;
        }

        public RetornoModel ValidaHorario(Horario horario)
        {
            if (horario.Hora.Hour > 24 || horario.Hora.Hour < 0)
            {
                return new RetornoModel() { Mensagem = "Hora inválida", Sucesso = false };
            }
            if (horario.DiaSemana > 6 || horario.DiaSemana < 0)
            {
                return new RetornoModel() { Mensagem = "Dia da semana inválido", Sucesso = false };
            }
            return new RetornoModel() { Mensagem = "Ok", Sucesso = true };
        }

        public List<Horario> ListarHorarios()
        {
            return dal.ListarHorarios();
        }
    }
}
