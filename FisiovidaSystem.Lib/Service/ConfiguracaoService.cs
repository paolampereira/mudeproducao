﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Lib.Service
{
    public class ConfiguracaoService
    {
        private ConfiguracaoADO dal = new ConfiguracaoADO();

        public Configuracao Carregar()
        {
            return dal.Carregar();
        }

        public void Salvar(Configuracao config)
        {
            dal.Salvar(config);
        }

        public Configuracao CarregarVariaveisSite()
        {
            return dal.CarregarVariaveisSite();
        }
        public Configuracao CarregarVariaveisContato()
        {
            return dal.CarregarVariaveisContato();
        }
    }
}
