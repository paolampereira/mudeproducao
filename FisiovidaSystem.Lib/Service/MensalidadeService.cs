﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class MensalidadeService
    {
        public int TotalRegistros { get; set; }
        private MensalidadeADO dal = new MensalidadeADO();

        public RetornoModel RegistrarPagamento(int alunoID, string descontos, double valorFinal)
        {
            RetornoModel retorno = new RetornoModel() { Sucesso = true, Mensagem = "Ok" };
            //carregar planos ativos
            AlunoService aService = new AlunoService();
            Aluno aluno = aService.Carregar(alunoID);
            /*
            PlanoService pservice = new PlanoService();
            pservice.ListarPlanosAluno(alunoID);
            */

            //carrega descontos
            var ids = descontos.Split(',').Select(Int32.Parse).ToList();
            DescontoService dservice = new DescontoService();
            List<Desconto> desc = dservice.CarregarDescontos(ids);

            double Valorfinal = aluno.MensalidadesAtrasadas.Sum(m => m.Valor);
            Valorfinal += aluno.Planos.Sum(p => p.ValorModalidade.Valor);
            Valorfinal -= desc.Sum(d => d.Porcentagem) * Valorfinal;

            if (Valorfinal == valorFinal)
            {
                aluno.MensalidadesAtrasadas.ForEach(m => { m.AlunoID = alunoID; });
                // registrando os pagamentos atrasados..;
                foreach (Mensalidade m in aluno.MensalidadesAtrasadas)
                {
                    m.Descontos = desc;
                    dal.RegistrarPagamento(m);
                }

                // registrando o pagamento do mes atual
                var totalMensalidadeAtual = aluno.Planos.Sum(p => p.ValorModalidade.Valor);
                dal.RegistrarPagamento(new Mensalidade()
                {
                    Valor = totalMensalidadeAtual,
                    Planos = aluno.Planos,
                    Referente = DateTime.Now,
                    Descontos = desc,
                    ValorPago = (totalMensalidadeAtual - desc.Sum(d => d.Porcentagem) * totalMensalidadeAtual),
                    DataPagamento = DateTime.Now
                });

                return retorno;
            }
            else
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Valores não batem" };
            }

        }

        public List<Mensalidade> ListarMensalidadesAluno(int alunoid)
        {
            return dal.ListarMensalidades(alunoid);
        }

    }
}
