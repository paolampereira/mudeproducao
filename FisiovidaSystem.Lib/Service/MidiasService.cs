﻿using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace FisiovidaSystem.Lib.Service
{
    public class MidiasService
    {
       

        public RetornoModel<string> Upload(HttpPostedFileBase arquivo)
        {
            string nomeArq = null;
            try
            {
                if ((arquivo != null) && (arquivo.ContentLength > 0))
                {
                    string content = arquivo.ContentType.Split('/')[0];
                    if (content == "image" || content == "video")
                    {
                        //Salva o arquivo 
                        nomeArq = Guid.NewGuid().ToString();
                        string nomeArqSalvar = nomeArq;
                        if (content == "image")
                        {
                            nomeArqSalvar += Path.GetExtension(arquivo.FileName);
                            nomeArq = nomeArqSalvar;
                        }
                        else
                        {
                            return new RetornoModel<string> { Mensagem = "Tipo de arquivo inválido.", Sucesso = false };
                        }
                        string caminhoArquivo = ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, nomeArqSalvar);
                        arquivo.SaveAs(caminhoArquivo);
                    }
                    else
                    {
                        return new RetornoModel<string> { Mensagem = "Tipo de arquivo inválido.", Sucesso = false };
                    }
                }
                else
                {
                    return new RetornoModel<string> { Mensagem = "Nenhum arquivo enviado.", Sucesso = false };
                }

            }
            catch (Exception e)
            {
                return new RetornoModel<string> { Mensagem = "Falha no upload, tente novamente!", Sucesso = false };
            }

            return new RetornoModel<string> { Retorno = nomeArq, Mensagem = "Upload realizado com sucesso", Sucesso = true };
        }
    }
}
