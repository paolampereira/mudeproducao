﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Lib.Service
{
    public class ValorModalidadeService
    {
        private ValorModalidadeADO dal = new ValorModalidadeADO();

        public ValorModalidade Carregar(int id)
        {
            return dal.Carregar(id);
        }

        public void Cadastrar(ValorModalidade entidade)
        {

            dal.Cadastrar(entidade);
        }

        public void Excluir(int id)
        {

            dal.Excluir(id);
        }

        public void Alterar(ValorModalidade entidade)
        {
            dal.Alterar(entidade);
        }

    }
}
