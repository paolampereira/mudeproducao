﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class TratamentoService
    {
        TratamentoADO dal = new TratamentoADO();
        public int TotalRegistros { get; set; }

        public Tratamento RegistrarImagens(List<string> arquivos, int tratamentoID)
        {
            Tratamento retorno = dal.RegistrarImagens(arquivos, tratamentoID);
            return retorno;
        }

        public bool Excluir(int id)
        {
            return dal.Excluir(id);
        }

        public void Salvar(Tratamento tratamento)
        {
            if (tratamento.ID > 0)
            {
                dal.Atualizar(tratamento);
            }
            else
            {
                dal.Inserir(tratamento);
            }
        }
        

        public List<Tratamento> ListarIdsNomes()
        {
            return dal.ListarIdsNomes();
        }

        public List<Tratamento> Listar(int skip, int take, string palavraChave)
        {
            List<Tratamento> retorno = dal.Listar(skip, take, palavraChave);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public Tratamento Carregar(int id)
        {
            return dal.Carregar(id);
        }
    }
}
