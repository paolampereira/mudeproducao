﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class TagService
    {
        private TagADO dal = new TagADO();
        public int TotalRegistros { get; set; }

        public List<Tag> Listar(int skip, int take, string palavraChave, bool diferencial)
        {
            List<Tag> retorno = dal.Listar(skip, take, palavraChave, diferencial);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public void Excluir(int id)
        {
            dal.Excluir(id);
        }

        public List<Tag> Listar()
        {
            List<Tag> retorno = dal.Listar();
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public List<Tag> ListarDiferenciais()
        {
            List<Tag> retorno = dal.ListarDiferenciais();
            return retorno;
        }

        public Tag Carregar(int id)
        {
            return dal.Carregar(id);
        }

        public RetornoModel Salvar(Tag tag)
        {
            RetornoModel retorno = ValidaTag(tag);
            if (retorno.Sucesso)
            {
                if (tag.ID > 0)
                {
                    dal.Alterar(tag);
                }
                else
                {
                    dal.Cadastrar(tag);
                }
            }

            return retorno;
        }

        private RetornoModel ValidaTag(Tag tag)
        {
            if (tag == null)
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Não é possível cadastrar registro vazio" };
            }
            else if (String.IsNullOrEmpty(tag.Nome) || String.IsNullOrEmpty(tag.Cor))
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Todos os campos devem ser preenchidos" };
            }
            else if (tag.Diferencial && string.IsNullOrEmpty(tag.Descricao))
            {
                return new RetornoModel() { Sucesso = false, Mensagem = "Todos os campos devem ser preenchidos" };
            }

            return new RetornoModel() { Sucesso = true, Mensagem = "Ok" };
        }
    }
}
