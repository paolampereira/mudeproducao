﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class InscricaoTempService
    {
        private InscricaoTempADO dal = new InscricaoTempADO();

        public void DesmarcarAula(InscricaoTemp inscricaoTemp)
        {
            dal.DesmarcarAula(inscricaoTemp);
        }

        public List<InscricaoTemp> ListarCancelamentosAluno(int alunoID)
        {
            return dal.ListarCancelamentosAluno(alunoID);
        }
        public List<InscricaoTemp> ListarCancelamentosComDetalhesAluno(int alunoID)
        {
            return dal.ListarCancelamentosComDetalhesAluno(alunoID);
        }

    }
}
