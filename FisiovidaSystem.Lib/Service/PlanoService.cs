﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class PlanoService
    {
        public int TotalRegistros { get; set; }
        private PlanoADO dal = new PlanoADO();

        public void SairTurma(int planoid, int turmaid, int usuarioid)
        {
            dal.SairTurma(turmaid, planoid, usuarioid);
        }


        public void CancelarPlano(int planoID)
        {
            dal.CancelarPlano(planoID);
        }


        public RetornoModel<Aluno> RealizarMatricula(Plano plano, int modalidadeID)
        {
            dal.RealizarMatricula(plano);

            AlunoService service = new AlunoService();
            Aluno aluno = service.Carregar(plano.AlunoID);
            return new RetornoModel<Aluno>() { Sucesso = true, Retorno = aluno };
        }

        public List<Plano> ListarPlanosAluno(int alunoid)
        {
            List<Plano> planos = dal.ListarPlanosAluno(alunoid);
            return planos;
        }

    }
}
