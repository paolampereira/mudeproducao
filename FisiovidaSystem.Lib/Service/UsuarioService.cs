﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Lib.Identity;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Util;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FisiovidaSystem.Lib.Service
{
    public class UsuarioService
    {
        private UsuarioADO dal = new UsuarioADO();
        public int TotalRegistros { get; set; }

        public List<Usuario> ListaProfessoresModalidade(int modalidadeID, int tratamentoID)
        {
            return dal.ListaProfessoresModalidade(modalidadeID, tratamentoID);
        }

        public Usuario Carregar(int id)
        {
            return dal.Carregar(id);
        }

        public Usuario Carregar(string username)
        {
            return dal.Carregar(username);
        }

        public RetornoModel Validar(Usuario usuario)
        {
            if(string.IsNullOrEmpty(usuario.Login) 
                ||string.IsNullOrEmpty(usuario.Nome) ||string.IsNullOrEmpty(usuario.CPF) ||
                string.IsNullOrEmpty(usuario.Email))
            {
                return new RetornoModel() { Mensagem = "Todos os campos devem ser preenchidos", Sucesso = false };
            }
            if ((string.IsNullOrEmpty(usuario.Senha) || string.IsNullOrEmpty(usuario.ConfirmarSenha)) && usuario.ID <= 0)
            {
                return new RetornoModel() { Mensagem = "Todos os campos devem ser preenchidos", Sucesso = false };
            }
            if (usuario.Senha != null)
            {
                if (usuario.Senha.Length < 6)
                {
                    return new RetornoModel() { Mensagem = "Senha deve ter pelo menos 6 caracteres", Sucesso = false };
                }
            }
            
            if (usuario.Senha != usuario.ConfirmarSenha && usuario.ID <= 0)
            {
                return new RetornoModel() { Mensagem = "Confirmação de senha errada", Sucesso = false };
            }
            if (!ValidationUtil.IsValidCPF(usuario.CPF))
            {
                return new RetornoModel() { Mensagem = "CPF inválido", Sucesso = false };
            }
            /*if (!ValidationUtil.IsValideMail(usuario.Email))
            {
                return new RetornoModel() { Mensagem = "Email inválido", Sucesso = false };
            }*/
            return new RetornoModel() { Mensagem = "Ok", Sucesso = true };
        }

        public bool Excluir(int id)
        {
            bool semTurmas = true;

            semTurmas = dal.Excluir(id);

            return semTurmas;
        }

        public Usuario Logar(string login, string senha)
        {
            Usuario retorno = dal.Logar(login, senha);

            if (retorno == null)
            {
                return null;
            }
            
            return retorno;
        }
        public List<Usuario> ListaProfessoresTurmas(int valorModalidadeID)
        {
            return dal.CarregarProfessoresTurmas(valorModalidadeID);
        }
        public void AlterarIsProfessor(Usuario usuario)
        {
            dal.AlterarIsProfessor(usuario);
        }
        public void AlterarIsMaster(Usuario usuario)
        {
            dal.AlterarIsMaster(usuario);
        }
        public void AlterarSenha(Usuario usuario)
        {
            dal.AlterarSenha(usuario);
        }

        public void Salvar(Usuario usuario)
        {
            usuario.CPF = usuario.CPF.Replace(".", "");
            usuario.CPF = usuario.CPF.Replace("-", "");

            if (usuario.ID > 0)
            {
                dal.Atualizar(usuario);
            }
            else
            {
                dal.Inserir(usuario);
            }
        }
        public List<Usuario> ListarIdsNomesProfessores()
        {
            return dal.ListarIdsNomesProfessores();
        }

        public List<Usuario> ListarProfessores()
        {
            return dal.ListarProfessores();
        }
        public List<Usuario> Listar(int skip, int take, string palavraChave)
        {
            List<Usuario> retorno = dal.Listar(skip, take, palavraChave);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public static bool TemPermissao(Usuario usuario, bool master)
        {
            if (usuario == null)
                return false;
            if (usuario.Master)
                return true;
            return (!master);
        }

        public ClaimsIdentity CriarIdentity(Usuario usuario)
        {
            var userManager = new UserManager<UsuarioIdentity>(new UsuarioUserStore(this));
            var identity = userManager.CreateIdentity(usuario.CopyTo(new UsuarioIdentity()), DefaultAuthenticationTypes.ApplicationCookie);
            return identity;
        }

        public async Task<ClaimsIdentity> CriarIdentityAsync(Usuario usuario)
        {
            var userManager = new UserManager<UsuarioIdentity>(new UsuarioUserStore(this));
            var identity = await userManager.CreateIdentityAsync(usuario.CopyTo(new UsuarioIdentity()), DefaultAuthenticationTypes.ApplicationCookie);
            return identity;
        }
    }
}
