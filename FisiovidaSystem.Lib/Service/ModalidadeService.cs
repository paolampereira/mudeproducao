﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FisiovidaSystem.Lib.Service
{
    public class ModalidadeService
    {
        ModalidadeADO dal = new ModalidadeADO();
        public int TotalRegistros { get; set; }

        // Lista todas modalidades e tratamentos..
        public List<ModalidadeTratamentoModel> ListarModalidadesSite()
        {
            return dal.ListarModalidadesSite();
        }

        public int TotalModalidadesTratamentos()
        {
            return dal.TotalModalidadesTratamentos();
        }

        public bool Excluir(int id)
        {
            return dal.Excluir(id);
        }

        public Imagem RemoverImagem(int id)
        {
            ImagemModalidadeADO ado = new ImagemModalidadeADO();
            Imagem i = ado.Excluir(id);
            return i;
        }

        public List<Modalidade> Listar(int skip, int take, string palavraChave)
        {
            List<Modalidade> retorno = dal.Listar(skip, take, palavraChave);
            TotalRegistros = dal.TotalRegistros;
            return retorno;
        }

        public void Salvar(Modalidade modalidade)
        {
            if (modalidade.ID > 0)
            {
                dal.Atualizar(modalidade);
            }
            else
            {
                dal.Inserir(modalidade);
            }
        }


        public Modalidade RegistrarImagens(List<string> arquivos, int modalidadeid)
        {
            Modalidade modalidade = dal.RegistrarImagens(arquivos, modalidadeid);
            return modalidade;
        }

        public Modalidade CarregarComValores(int id)
        {
            Modalidade retorno = dal.CarregarComValores(id);
            return retorno;
        }
        public Modalidade Carregar(int id)
        {
            Modalidade retorno = dal.Carregar(id);
            return retorno;
        }
        public List<Modalidade> ListarIdsNomes()
        {
            return dal.ListarIdsNomes();
        }
        public List<Modalidade> ListarComPlanos()
        {
            return dal.ListarComPlanos();
        }
    }
}
