﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Service;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    public class ContaController : MasterController
    {
        // GET: Conta
        public virtual ActionResult Index(string returnUrl = null)
        {
            return View();
        }

        public virtual ActionResult Entrar(UsuarioModel Usuario, string returnUrl = null)
        {
            UsuarioService service = new UsuarioService();

            SessionModel.Usuario = service.Logar(Usuario.Login, Usuario.Senha);
            if (SessionModel.Usuario != null)
            {
                SalvarSessao();

                var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = Usuario.Lembrarme }, service.CriarIdentity(SessionModel.Usuario));

                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", "Conta");
            }
        }
        [Authorize]
        public virtual ActionResult Sair()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Index", "Conta");
        }

    }
}