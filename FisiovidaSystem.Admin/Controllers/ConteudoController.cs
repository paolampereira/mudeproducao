﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FisiovidaSystem.Lib.Util;
using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Admin.Util;
using System.IO;

namespace FisiovidaSystem.Admin.Controllers
{
    [AdminAuthorize(true)]
    public class ConteudoController : MasterController
    {
        public ActionResult Tags()
        {
            return View();
        }
        public ActionResult Diferencial()
        {
            return View();
        }
        public ActionResult Novidade()
        {
            return View();
        }
        public ActionResult Icone()
        {
            return View();
        }
        public ActionResult EditarDiferencial(int id = 0)
        {
            Tag entidade = new Tag();

            if (id > 0)
            {
                TagService service = new TagService();
                try
                {
                    entidade = service.Carregar(id);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Diferencial", "Conteudo", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
                }

            }
            return View(entidade);
        }
        public ActionResult EditarIcone(int id = 0)
        {
            Icone entidade = new Icone();

            if (id > 0)
            {
                IconeService service = new IconeService();
                try
                {
                    entidade = service.Carregar(id);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Icone", "Conteudo", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
                }

            }
            return View(entidade);
        }

        public ActionResult EditarTag(int id = 0)
        {
            Tag entidade = new Tag();

            if (id > 0)
            {
                TagService service = new TagService();
                try
                {
                    entidade = service.Carregar(id);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Tags", "Conteudo", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
                }

            }
            return View(entidade);
        }

        public ActionResult EditarNovidade(int id = 0)
        {
            Novidade entidade = new Novidade();

            NovidadeService service = new NovidadeService();
            try
            {
                entidade.Tags = new List<Tag>();
                entidade.Galeria = new List<Imagem>();
                if (id > 0)
                {
                    entidade = service.Carregar(id);
                }
                TagService tService = new TagService();
                entidade.TagsSistema = tService.Listar();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Novidade", "Conteudo", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
            }


            return View(entidade);
        }

        public ActionResult SalvarTag(Tag tag)
        {
            TagService service = new TagService();
            try
            {
                RetornoModel retorno = service.Salvar(tag);

                if (!retorno.Sucesso)
                {
                    return RedirectToAction("Tags", "Conteudo", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Tags", "Conteudo", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
            }
            return RedirectToAction("Tags", "Conteudo", new { msg = "Tag cadastrada com sucesso!", erro = false });
        }

        public ActionResult SalvarDiferencial(Tag tag)
        {
            TagService service = new TagService();
            try
            {
                tag.Diferencial = true;
                RetornoModel retorno = service.Salvar(tag);

                if (!retorno.Sucesso)
                {
                    return RedirectToAction("Diferencial", "Conteudo", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Diferencial", "Conteudo", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
            }
            return RedirectToAction("Diferencial", "Conteudo", new { msg = "Diferencial cadastrado com sucesso!", erro = false });
        }

        [ValidateInput(false)]
        public ActionResult SalvarNovidade(Novidade novidade)
        {
            NovidadeService service = new NovidadeService();
            try
            {
                novidade.Usuario = SessionModel.Usuario;
                novidade.UsuarioID = novidade.Usuario.ID;

                MidiasService mService = new MidiasService();

                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    RetornoModel<string> retorno = mService.Upload(file);
                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                }
                if (arquivos.Count == 1)
                {
                    if (!string.IsNullOrEmpty(novidade.Imagem))
                    {
                        System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, novidade.Imagem));
                    }
                    novidade.Imagem = arquivos[0];
                    //cortando imagem capa..
                    if (!string.IsNullOrEmpty(novidade.ImagemCapa))
                    {
                        System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, novidade.ImagemCapa));
                    }
                    novidade.ImagemCapa = Guid.NewGuid().ToString();
                    novidade.ImagemCapa += Path.GetExtension(novidade.Imagem);

                    Crop(novidade.Imagem, novidade.x, novidade.y, novidade.w, novidade.h, novidade.ImagemCapa);

                    //cortando imagem destaque..
                    if (novidade.Destaque)
                    {
                        if (!string.IsNullOrEmpty(novidade.ImagemDestaque))
                        {
                            System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, novidade.ImagemDestaque));
                        }
                        novidade.ImagemDestaque = Guid.NewGuid().ToString();
                        novidade.ImagemDestaque += Path.GetExtension(novidade.Imagem);
                        Crop(novidade.Imagem, novidade.x1, novidade.y1, novidade.w1, novidade.h1, novidade.ImagemDestaque);
                    }
                }
                else if (novidade.EditouCrop && novidade.Destaque)
                {
                    //cortando imagem destaque..
                    if (!string.IsNullOrEmpty(novidade.ImagemDestaque))
                    {
                        System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, novidade.ImagemDestaque));
                    }
                    novidade.ImagemDestaque = Guid.NewGuid().ToString();
                    novidade.ImagemDestaque += Path.GetExtension(novidade.Imagem);
                    Crop(novidade.Imagem, novidade.x1, novidade.y1, novidade.w1, novidade.h1, novidade.ImagemDestaque);
                }
                else if (novidade.EditouCapa)
                {
                    //cortando imagem capa..
                    if (!string.IsNullOrEmpty(novidade.ImagemCapa))
                    {
                        System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, novidade.ImagemCapa));
                    }
                    novidade.ImagemCapa = Guid.NewGuid().ToString();
                    novidade.ImagemCapa += Path.GetExtension(novidade.Imagem);

                    Crop(novidade.Imagem, novidade.x, novidade.y, novidade.w, novidade.h, novidade.ImagemCapa);
                }

                RetornoModel retorno2 = service.Salvar(novidade);

                if (!retorno2.Sucesso)
                {
                    return RedirectToAction("Novidade", "Conteudo", new { msg = "Ocorreu um erro ao salvar a notícia", erro = true });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Novidade", "Conteudo", new { msg = "Ocorreu um erro ao salvar a notícia", erro = true });
            }
            return RedirectToAction("Novidade", "Conteudo", new { msg = "Nótícia salva com sucesso!", erro = false });
        }

        private void Crop(string fileName, int x, int y, int width, int height, string saveIn)
        {
            /*string CompletePath = Url.Content(FisiovidaSystem.Lib.Util.ConfiguracaoAppUtil.Get(FisiovidaSystem.Lib.Enumerator.enumConfiguracaoLib.MidiasUrlCrop) + fileName);
            byte[] imageBytes = System.IO.File.ReadAllBytes(Server.MapPath(CompletePath));
            byte[] croppedImage = MidiaHelper.CropImage(imageBytes, x, y, width, height);
            */
            string rootPath = Server.MapPath("~");
            string path = Path.GetFullPath(Path.Combine(rootPath, FisiovidaSystem.Lib.Util.ConfiguracaoAppUtil.Get(FisiovidaSystem.Lib.Enumerator.enumConfiguracaoLib.MidiasUrlCrop) + fileName));

            byte[] imageBytes = System.IO.File.ReadAllBytes(path);
            byte[] croppedImage = MidiaHelper.CropImage(imageBytes, x, y, width, height);

            System.IO.File.WriteAllBytes(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, saveIn), croppedImage);
        }

        public ActionResult SalvarIcone(Icone icone)
        {
            IconeService service = new IconeService();
            try
            {
                MidiasService mService = new MidiasService();

                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    RetornoModel<string> retorno = mService.Upload(file);
                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                }
                if (arquivos.Count == 1)
                {
                    icone.Imagem = arquivos[0];
                }

                service.Salvar(icone);

            }
            catch (Exception ex)
            {

                return RedirectToAction("Icone", "Conteudo", new { msg = "Ocorreu um erro ao salvar o ícone!", erro = true });
            }
            return RedirectToAction("Icone", "Conteudo", new { msg = "Ícone salvo com sucesso!", erro = false });
        }


        public ActionResult RemoverImagem(int id)
        {
            try
            {
                NovidadeService service = new NovidadeService();
                service.RemoverImagem(id);
            }
            catch (Exception ex)
            {
                return Json(new { sucesso = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { sucesso = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListarTags(jQueryDataTableParamModel param)
        {
            IList<Tag> result = new List<Tag>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;

                TagService service = new TagService();

                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave, false);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                listaDados.Add(new string[] {
                    item.Nome,
                    item.Cor,
                    item.ID.ToString(),
                    item.ID.ToString()
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;
        }

        public ActionResult ListarIcones(jQueryDataTableParamModel param)
        {
            IList<Icone> result = new List<Icone>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;

                IconeService service = new IconeService();

                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                listaDados.Add(new string[] {
                    item.Nome,
                    item.ID.ToString(),
                    item.ID.ToString()
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;
        }


        public ActionResult ListarDiferenciais(jQueryDataTableParamModel param)
        {
            IList<Tag> result = new List<Tag>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;

                TagService service = new TagService();

                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave, true);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                listaDados.Add(new string[] {
                    item.Nome,
                    item.Cor,
                    item.ID.ToString(),
                    item.ID.ToString()
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;
        }


        public ActionResult ListarNovidades(jQueryDataTableParamModel param)
        {
            IList<Novidade> result = new List<Novidade>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;

                NovidadeService service = new NovidadeService();

                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                listaDados.Add(new string[] {
                    item.Titulo,
                    item.DataCadastro.ToString("dd/MM/yyyy"),
                    item.UsuarioID.ToString(),
                    item.ID.ToString(),
                    item.ID.ToString()
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;
        }

        public ActionResult ExcluirNovidade(int id)
        {
            try
            {
                NovidadeService service = new NovidadeService();
                service.Excluir(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Novidade", "Conteudo", new { msg = "Ocorreu um erro ao excluir a notícia!", erro = true });
            }
            return RedirectToAction("Novidade", "Conteudo", new { msg = "Notícia excluída com sucesso!", erro = false });
        }

        public ActionResult ExcluirIcone(int id)
        {
            try
            {
                IconeService service = new IconeService();
                service.Excluir(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Icone", "Conteudo", new { msg = "Ocorreu um erro ao excluir o ícone!", erro = true });
            }
            return RedirectToAction("Icone", "Conteudo", new { msg = "Ícone excluído com sucesso!", erro = false });
        }

        public ActionResult ExcluirTag(int id)
        {
            try
            {
                TagService service = new TagService();
                service.Excluir(id);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Icone", "Conteudo", new { msg = "Ocorreu um erro ao excluir o ícone!", erro = true });
            }
            return RedirectToAction("Icone", "Conteudo", new { msg = "Ícone excluído com sucesso!", erro = false });
        }

        public ActionResult UploadImagens(int novidadeID)
        {
            bool algumErro = false;
            Novidade novidade;
            try
            {
                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    MidiasService mService = new MidiasService();
                    RetornoModel<string> retorno = mService.Upload(file);
                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                    else
                    {
                        algumErro = true;
                    }
                }
                if (arquivos.Count > 0)
                {
                    NovidadeService service = new NovidadeService();
                    novidade = service.RegistrarImagens(arquivos, novidadeID);
                }
                else
                {
                    return Json(new { sucesso = false }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                string a = ex.Message;
                return Json(new { sucesso = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { sucesso = true, msg = RenderRazorViewToString("_GaleriaPartial", novidade), algumerro = algumErro }, JsonRequestBehavior.AllowGet);
        }
    }
}