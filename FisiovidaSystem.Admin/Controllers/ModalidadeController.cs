﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Admin.Util;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [AdminAuthorize(true)]
    public class ModalidadeController : MasterController
    {
        ValorModalidadeService vmService = new ValorModalidadeService();
        ModalidadeService service = new ModalidadeService();
        // GET: Modalidade
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Excluir(int id)
        {
            try
            {
                bool result = service.Excluir(id);
                if (!result)
                {
                    return RedirectToAction("Index", "Modalidade", new { erro = true, msg = "A modalidade não pode ser excluída pois contém turmas ativas" });
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Modalidade", new { erro = true, msg = "Ocorreu um erro ao excluir a modalidade" });
            }
            return RedirectToAction("Index", "Modalidade", new { erro = false, msg = "Modalidade excluída com sucesso!" });
        }

        public ActionResult UploadImagens(int modalidadeID)
        {
            bool algumErro = false;
            Modalidade modalidade;
            try
            {

                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    MidiasService mService = new MidiasService();
                    RetornoModel<string> retorno = mService.Upload(file);
                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                    else
                    {
                        algumErro = true;
                    }
                }
                if (arquivos.Count > 0)
                {
                    modalidade = service.RegistrarImagens(arquivos, modalidadeID);
                }
                else
                {
                    return Json(new { sucesso = false }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                string a = ex.Message;
                return Json(new { sucesso = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { sucesso = true, msg = RenderRazorViewToString("_GaleriaPartial", modalidade), algumerro = algumErro }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detalhe(int id)
        {
            Modalidade entidade = new Modalidade();
            try
            {
                entidade = service.CarregarComValores(id);
            }
            catch (Exception ex)
            {

            }
            return View(entidade);
        }

        public ActionResult CarregarProfessoresTurmas(int valorModalidade)
        {
            UsuarioService service = new UsuarioService();
            ProfessoresTurmaModel model = new ProfessoresTurmaModel();
            try
            {
                model.Professores = service.ListaProfessoresTurmas(valorModalidade);
            }
            catch (Exception ex)
            {

            }
            return PartialView("_ProfessoresTurmaPartial", model);
        }

        private void Crop(string fileName, int x, int y, int width, int height)
        {
            string rootPath = Server.MapPath("~");
            string path = Path.GetFullPath(Path.Combine(rootPath, FisiovidaSystem.Lib.Util.ConfiguracaoAppUtil.Get(FisiovidaSystem.Lib.Enumerator.enumConfiguracaoLib.MidiasUrlCrop) + fileName));

            byte[] imageBytes = System.IO.File.ReadAllBytes(path);
            byte[] croppedImage = MidiaHelper.CropImage(imageBytes, x, y, width, height);

            System.IO.File.WriteAllBytes(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, fileName), croppedImage);
        }

        [HttpPost]
        public ActionResult Salvar(Modalidade modalidade)
        {
            try
            {
                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    MidiasService mService = new MidiasService();
                    RetornoModel<string> retorno = mService.Upload(file);
                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                }
                if (arquivos.Count == 1)
                {
                    if (!string.IsNullOrEmpty(modalidade.FotoCapa))
                    {
                        System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, modalidade.FotoCapa));
                    }
                    modalidade.FotoCapa = arquivos[0];
                    Crop(modalidade.FotoCapa, modalidade.x, modalidade.y, modalidade.w, modalidade.h);
                }

                service.Salvar(modalidade);

            }
            catch (Exception ex)
            {
                if (modalidade.ID > 0)
                {
                    return RedirectToAction("Detalhe", "Modalidade", new { msg = "Erro ao salvar os dados!", erro = true, id = modalidade.ID });
                }
                return RedirectToAction("Index", "Modalidade", new { msg = "Erro ao salvar os dados!", erro = true });
            }
            if (modalidade.ID > 0)
            {
                return RedirectToAction("Detalhe", "Modalidade", new { msg = "Modalidade editada com sucesso!", erro = false, id = modalidade.ID });
            }
            return RedirectToAction("Index", "Modalidade", new { msg = "Modalidade cadastrada com sucesso!", erro = false });
        }

        public ActionResult NovoPacote(int id, string nome)
        {
            PacoteModalidadeModel model = new PacoteModalidadeModel();
            model.ModalidadeID = id;
            model.ModalidadeNome = nome;
            return View(model);
        }

        public ActionResult Editar(int id = 0)
        {
            Modalidade entidade = new Modalidade();
            entidade.Icones = new List<Icone>();
            entidade.IconesSistema = new List<Icone>();
            entidade.TagsIds = new List<int>();
            try
            {
                if (id > 0)
                {
                    entidade = service.Carregar(id);
                }
                IconeService iService = new IconeService();
                entidade.IconesSistema = iService.Listar();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Modalidade", new { erro = true, msg = "Ocorreu um erro ao carregar os dados" });
            }

            return View(entidade);
        }

        public ActionResult RemoverImagem(int id)
        {
            try
            {
                Imagem img = service.RemoverImagem(id);
                System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, img.Arquivo));
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, msg = "Ocorreu um erro ao excluir a imagem!" });
            }
            return Json(new { sucesso = true, msg = "Imagem excluída com sucesso!" });
        }

        public ActionResult EditarPacote(int id, string nome)
        {
            PacoteModalidadeModel pacote = new PacoteModalidadeModel();
            try
            {
                ValorModalidade v = vmService.Carregar(id);
                pacote.ModalidadeID = v.ModalidadeID;
                pacote.QtdDias = v.QtdDias;
                pacote.Valor = v.Valor;
                pacote.Descricao = v.Descricao;
                pacote.ModalidadeNome = nome;
                pacote.ID = v.ID;
            }
            catch (Exception e)
            {

            }
            return View(pacote);
        }

        public ActionResult ExcluirPacote(int id)
        {
            try
            {
                vmService.Excluir(id);
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, msg = "Ocorreu um erro ao tentar excluir o pacote!" });
            }
            return Json(new { sucesso = true, msg = "Pacote excluído com sucesso!" });
        }

        public ActionResult SalvarPacote(PacoteModalidadeModel model)
        {
            try
            {
                ValorModalidade pacote = new ValorModalidade()
                {
                    ModalidadeID = model.ModalidadeID,
                    Descricao = model.Descricao,
                    QtdDias = model.QtdDias,
                    Valor = model.Valor,
                    ID = model.ID
                };
                if (model.ID == 0)
                {
                    vmService.Cadastrar(pacote);
                }
                else
                {
                    vmService.Alterar(pacote);
                }

            }
            catch (Exception e)
            {
                if (e.Message == "23505: duplicate key value violates unique constraint \"valormodalidade_modalidadeid_qtddias_valor_key\"")
                {
                    return RedirectToAction("EditarPacote", "Modalidade", new { id = model.ID, erro = true, msg = "Esse pacote já existe para essa modalidade!" });
                }
                if (model.ID == 0)
                {
                    return RedirectToAction("NovoPacote", "Modalidade", new { modalidadeid = model.ModalidadeID, erro = true, msg = "Ocorreu um erro ao salvar os dados!" });
                }
                return RedirectToAction("EditarPacote", "Modalidade", new { id = model.ID, erro = true, msg = "Ocorreu um erro ao salvar os dados!" });
            }
            return RedirectToAction("Detalhe", "Modalidade", new { id = model.ModalidadeID, erro = false, msg = "Pacote cadastrado com sucesso!" });
        }

        public ActionResult Listar(jQueryDataTableParamModel param)
        {
            IList<Modalidade> result = new List<Modalidade>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;


                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                var descr = item.Descricao;

                if (item.Descricao.Length > 100)
                {
                    descr = item.Descricao.Substring(0, 100) + "..";
                }

                listaDados.Add(new string[] {
                    item.Nome,
                    descr,
                    item.Duracao.ToString() + " min",
                    item.ID.ToString(),
                    item.ID.ToString() 
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;


        }
    }
}