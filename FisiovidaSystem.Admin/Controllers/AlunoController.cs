﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [Authorize]
    public class AlunoController : MasterController
    {
        // GET: Aluno
        public ActionResult Index()
        {
            return View();
        }
        [AdminAuthorize(true)]
        public ActionResult SairTurma(int planoid, int turmaid)
        {
            AlunoService service = new AlunoService();
            try
            {
                service.SairTurma(this.SessionModel.Usuario.ID, turmaid, planoid);
            }
            catch (Exception ex)
            {
                string msg = "Ocorreu um erro ao sair da turma";
                if (ex.Message == "Não há planos disponíveis com um dia a menos.")
                {
                    msg = "Não há planos disponíveis com um dia a menos.";
                }
                return Json(new { sucesso = false, msg = msg }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { sucesso = true }, JsonRequestBehavior.AllowGet);
        }

        [AdminAuthorize(true)]
        public ActionResult ConcluirMatricula(string turmas, int alunoID, int planoID, int professorID, int modalidadeID)
        {
            var idsTurmas = turmas.Split(',').Select(Int32.Parse).ToList();
            PlanoService service = new PlanoService();
            Plano plano = new Plano()
            {
                DataInicio = DateTime.Now,
                AlunoID = alunoID,
                ValorModalidadeID = planoID,
                TurmasIDs = idsTurmas,
                ProfessorID = professorID
            };
            Aluno aluno = new Aluno();
            try
            {
                RetornoModel<Aluno> retorno = service.RealizarMatricula(plano, modalidadeID);
                if (!retorno.Sucesso)
                {
                    return Json(new { sucesso = false, msg = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
                }
                aluno = retorno.Retorno;
                if (plano == null)
                {
                    return Json(new { sucesso = false, msg = "Erro ao realizar matrícula" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { sucesso = false, msg = "Erro ao realizar matrícula" }, JsonRequestBehavior.AllowGet);
            }
            //TODO enviar partial das mensalidades e dos saldos

            AlunoModel model = new AlunoModel() { Aluno = aluno };
            return Json(new { sucesso = true, msg = RenderRazorViewToString("_PlanosAlunoPartial", model) }, JsonRequestBehavior.AllowGet);
        }
        [AdminAuthorize(true)]
        public ActionResult RegistrarPagamento(int alunoID, string descontos, decimal valorFinal)
        {
            MensalidadeService mService = new MensalidadeService();
            try
            {
                RetornoModel retorno = mService.RegistrarPagamento(alunoID, descontos, (double)valorFinal);
                if (!retorno.Sucesso)
                {
                    return Json(new { sucesso = false, msg = "Ocorreu um erro ao registrar o pagamento" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { sucesso = false, msg = "Ocorreu um erro ao registrar o pagamento" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { sucesso = true }, JsonRequestBehavior.AllowGet);
        }


        [AdminAuthorize(true)]
        public ActionResult FecharPlano(int planoID)
        {
            PlanoService planoService = new PlanoService();
            try
            {
                planoService.CancelarPlano(planoID);
            }
            catch (Exception ex)
            {
                return Json(new { sucesso = false, msg = "Ocorreu um erro ao cancelar o plano" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { sucesso = true }, JsonRequestBehavior.AllowGet);
        }
        [AdminAuthorize(true)]
        public ActionResult Editar(int id = 0)
        {
            Aluno entidade = new Aluno();

            if (id > 0)
            {
                AlunoService service = new AlunoService();
                try
                {
                    entidade = service.CarregarDados(id);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Detalhe", "Aluno", new { id = id, msg = "Ocorreu um erro ao carregar os dados", erro = true });
                }

            }
            return View(entidade);
        }
        [AdminAuthorize(true)]
        public ActionResult Detalhe(int id)
        {
            AlunoModel model = new AlunoModel();
            model.Aluno = new Aluno();
            model.Modalidades = new List<Modalidade>();

            AlunoService service = new AlunoService();
            ModalidadeService mService = new ModalidadeService();
            DescontoService dService = new DescontoService();
            try
            {
                int cont;
                model.Aluno = service.CarregarDetalhes(id);
                model.Modalidades = mService.ListarComPlanos();
                model.Descontos = dService.ListarAtivos();

                if (Session["aba"] != null && Session["cont"] != null)
                {
                    cont = (int)Session["cont"];
                    Session["cont"] = cont + 1;
                    if (cont == 1)
                    {
                        model.Aba = (int)Session["aba"];
                    }
                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Aluno", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
            }

            return View(model);
        }
        [AdminAuthorize(true)]
        public ActionResult Excluir(int id)
        {
            try
            {
                AlunoService service = new AlunoService();

                if (id == SessionModel.Usuario.ID)
                {
                    return RedirectToAction("Index", "Aluno", new { erro = true, msg = "Você não pode excluir seu próprio perfil!" });
                }
                service.Excluir(id);
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Aluno", new { erro = true, msg = "Ocorreu um erro inesperado ao excluir o usuário" });
            }
            return RedirectToAction("Index", "Aluno", new { erro = false, msg = "Usuário excluído com sucesso!" });
        }
        [AdminAuthorize(true)]
        public ActionResult Salvar(Aluno aluno)
        {
            AlunoService service = new AlunoService();
            try
            {
            RetornoModel retorno = service.Salvar(aluno);

            if (!retorno.Sucesso)
            {
                return RedirectToAction("Index", "Aluno", new { msg = "Ocorreu um erro ao salvar os dados!", erro = true });
            }
            }
            catch (Exception ex)
            {
                if ("23505: duplicate key value violates unique constraint \"aluno_cpf_key\"" == ex.Message)
                {
                    return Json(new RetornoModel() { Sucesso = false, Mensagem = "CPF já existente no sistema" }, JsonRequestBehavior.AllowGet);
                }
                if ("23505: duplicate key value violates unique constraint \"aluno_login_key\"" == ex.Message)
                {
                    return Json(new RetornoModel() { Sucesso = false, Mensagem = "Login já existente no sistema" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new RetornoModel() { Sucesso = false, Mensagem = "Ocorreu um erro ao salvar os dados" }, JsonRequestBehavior.AllowGet);
            }
            if (aluno.ID > 0)
            {
                return RedirectToAction("Detalhe", "Aluno", new { msg = "Dados salvos com sucesso!", erro = false, id = aluno.ID });
            }
            return RedirectToAction("Index", "Aluno", new { msg = "Dados salvos com sucesso!", erro = false });
        }
        [AdminAuthorize(true)]
        public ActionResult Recuperar(int alunoid, int planoid)
        {
            return View();
        }
        [AdminAuthorize(true)]
        public ActionResult Desmarcar(int alunoid, int turmaid, int planoid, string restanteSemana)
        {
            DesmarcarModel model = new DesmarcarModel();
            model.AlunoID = alunoid;
            model.TurmaID = turmaid;
            model.PlanoID = planoid;

            AlunoService service = new AlunoService();
            try
            {
                model.Cancelamentos = service.ListarCancelamentosAlunoTurma(alunoid, turmaid);

                Aluno a = new Aluno();
                a.ID = alunoid;
                a.Cancelamentos = model.Cancelamentos;
                model.StringCancelamentos = a.CancelamentosTurma(turmaid);
                model.RestanteSemana = restanteSemana;
            }
            catch (Exception ex)
            {
                return RedirectToAction("Detalhe", "Aluno", new { id = alunoid, msg = "Ocorreu um erro ao carregar os dados!", erro = true });
            }

            return View(model);
        }
        [AdminAuthorize(true)]
        public ActionResult DesmarcarAula(string dia, int alunoID, int turmaID, int planoid)
        {
            DateTime diaCancel = new DateTime(Int32.Parse(dia.Split('/')[2]), Int32.Parse(dia.Split('/')[1]), Int32.Parse(dia.Split('/')[0]));

            InscricaoTempService inscricaoTempService = new InscricaoTempService();
            try
            {
                inscricaoTempService.DesmarcarAula(new InscricaoTemp()
                {
                    Dia = diaCancel,
                    AlunoID = alunoID,
                    TurmaID = turmaID,
                    PlanoID = planoid,
                    Desmarcar = true
                });
                Session["aba"] = 1;
                Session["cont"] = 1;
            }
            catch (Exception ex)
            {
                if (ex.Message == "23505: duplicate key value violates unique constraint \"inscricaotemp_alunoid_turmaid_dia_desmarcar_key\"")
                {
                    return RedirectToAction("Detalhe", "Aluno", new { id = alunoID, msg = "Aula nesta data já está cancelada!", erro = true });
                }
                return RedirectToAction("Detalhe", "Aluno", new { id = alunoID, msg = "Ocorreu um erro ao desmarcar a aula!", erro = true });
            }
            return RedirectToAction("Detalhe", "Aluno", new { id = alunoID, msg = "Aula desmarcada com sucesso!", erro = false });
        }

        public ActionResult Listar(jQueryDataTableParamModel param)
        {
            IList<Aluno> result = new List<Aluno>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;

                AlunoService service = new AlunoService();

                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {
                return RedirectToAction("InternalServer", "Erro", new { erro = true, msg = "Ocorreu um erro inesperado ao carregar os dados" });
            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                listaDados.Add(new string[] {
                    item.Nome,
                    item.Email,
                    item.Login,
                    item.TelefonesFormatados,
                    String.Format("{0}.{1}.{2}-{3}", item.CPF.Substring(0, 3), item.CPF.Substring(3, 3), item.CPF.Substring(5, 3), item.CPF.Substring(9)),
                    item.ID.ToString(),
                    item.ID.ToString(),
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;
        }
    }
}