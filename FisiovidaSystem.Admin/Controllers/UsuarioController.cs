﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Admin.Util;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [Authorize]
    public class UsuarioController : MasterController
    {
        UsuarioService service = new UsuarioService();

        [AdminAuthorize(true)]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MinhasTurmas()
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
            return View();
        }

        public ActionResult MeuPerfil(int id)
        {
            Usuario usuario = new Usuario();
            try
            {
                usuario = SessionModel.Usuario;
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Home", new { erro = true, msg = "Ocorreu um erro inesperado ao carregar os dados" });
            }
            return View(usuario);
        }
        [AdminAuthorize(true)]
        public ActionResult Excluir(int id)
        {
            try
            {
                if (id == SessionModel.Usuario.ID)
                {
                    return RedirectToAction("Index", "Usuario", new { erro = true, msg = "Você não pode excluir seu próprio perfil!" });
                }
                bool semTurmasPlanos = service.Excluir(id);
                if (!semTurmasPlanos)
                {
                    return RedirectToAction("Index", "Usuario", new { erro = true, msg = "Este usuário não pode ser exclúido. Verifique se ele não possui alunos/turmas" });
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Usuario", new { erro = true, msg = "Ocorreu um erro inesperado ao excluir o usuário" });
            }
            return RedirectToAction("Index", "Usuario", new { erro = false, msg = "Usuário excluído com sucesso!"});
        }

        private void Crop(string fileName, int x, int y, int width, int height)
        {
            string rootPath = Server.MapPath("~");
            string path = Path.GetFullPath(Path.Combine(rootPath, FisiovidaSystem.Lib.Util.ConfiguracaoAppUtil.Get(FisiovidaSystem.Lib.Enumerator.enumConfiguracaoLib.MidiasUrlCrop) + fileName));

            byte[] imageBytes = System.IO.File.ReadAllBytes(path);
            byte[] croppedImage = MidiaHelper.CropImage(imageBytes, x, y, width, height);

            System.IO.File.WriteAllBytes(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, fileName), croppedImage);
        }
        [AdminAuthorize(true)]
        [HttpPost]
        public ActionResult Salvar(Usuario usuario)
        {
            try
            {
                RetornoModel validacao = service.Validar(usuario);
                if (!validacao.Sucesso)
                {
                    return RedirectToAction("Index", "Usuario", new { erro = true, msg = validacao.Mensagem });
                }

                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    MidiasService mService = new MidiasService();
                    HttpPostedFileBase file = Request.Files[fileName];
                    RetornoModel<string> retorno = mService.Upload(file);
                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                }

                bool foto = false;
                if (arquivos.Count == 1)
                {
                    usuario.Foto = arquivos[0];
                    foto = true;
                    Crop(usuario.Foto, usuario.x, usuario.y, usuario.w, usuario.h);
                }

                service.Salvar(usuario);
                if (foto && usuario.ID == SessionModel.Usuario.ID)
                {
                    SessionModel.Usuario.Foto = usuario.Foto;
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Usuario", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
            }
            return RedirectToAction("Index", "Usuario", new { msg = "Dados salvos com sucesso!", erro = false });
        }

        [AdminAuthorize(true)]
        public ActionResult AlterarSenha(Usuario usuario)
        {
            try
            {
                if (string.IsNullOrEmpty(usuario.Senha) || string.IsNullOrEmpty(usuario.ConfirmarSenha))
                {
                    return RedirectToAction("Index", "Usuario", new { erro = true, msg = "Os campos senha e confirmação devem ser preenchidos!" });
                }
                if (usuario.Senha != usuario.ConfirmarSenha)
                {
                    return RedirectToAction("Index", "Usuario", new { erro = true, msg = "Os campos senha e confirmação devem ser iguais!" });
                }
                if (usuario.Senha.Length < 6)
                {
                    return RedirectToAction("Index", "Usuario", new { erro = true, msg = "A senha deve ter no mínimo 6 caracteres!" });
                }

                // salvando..
                service.AlterarSenha(usuario);

            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Usuario", new { erro = true, msg = "Ocorreu um erro ao salvar os dados!" });
            }
            return RedirectToAction("Index", "Usuario", new { erro = false, msg = "Senha alterada com sucesso!" });
        }


        [AdminAuthorize(true)]
        public ActionResult Editar(int id = 0)
        {
            Usuario usuario = new Usuario();
            try
            {
                if (id > 0)
                {
                    usuario = service.Carregar(id);
                }


            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Usuario", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
            }
            return View(usuario);
        }
        [AdminAuthorize(true)]
        public ActionResult SetProfessor(int id, bool professor)
        {
            try
            {
                Usuario usuario = new Usuario() { ID = id, Professor = professor };
                service.AlterarIsProfessor(usuario);
            }
            catch (Exception e)
            {
                return Json(new RetornoModel() { Sucesso = false, Mensagem = "Erro ao salvar os dados" });
            }

            return Json(new RetornoModel() { Sucesso = true }, JsonRequestBehavior.AllowGet);
        }
        [AdminAuthorize(true)]
        public ActionResult SetMaster(int id, bool master)
        {
            try
            {
                Usuario usuario = new Usuario() { ID = id, Master = master };
                service.AlterarIsMaster(usuario);
            }
            catch (Exception e)
            {
                return Json(new RetornoModel() { Sucesso = false, Mensagem = "Erro ao salvar os dados" });
            }

            return Json(new RetornoModel() { Sucesso = true }, JsonRequestBehavior.AllowGet);
        }
        [AdminAuthorize(true)]
        public ActionResult Listar(jQueryDataTableParamModel param)
        {
            IList<Usuario> result = new List<Usuario>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;

                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;

                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {
                return RedirectToAction("InternalServer", "Erro", new { erro = true, msg = "Ocorreu um erro inesperado ao carregar os dados" });
            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                listaDados.Add(new string[] {
                    item.Nome,
                    item.Email,
                    item.Login,
                    item.Professor + "-" + item.ID,
                    item.Master + "-" + item.ID,
                    item.ID.ToString(),
                    item.ID.ToString(),
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;
        }

    }
}