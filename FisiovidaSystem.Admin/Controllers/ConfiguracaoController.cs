﻿using FisiovidaSystem.Admin.Util;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [AdminAuthorize(true)]
    public class ConfiguracaoController : MasterController
    {
        private ConfiguracaoService service = new ConfiguracaoService();

        public ActionResult Index()
        {
            Configuracao config = new Configuracao();

            try
            {
                config = service.Carregar();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Configuracao", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
            }
            return View(config);
        }
        private void Crop(string fileName, int x, int y, int width, int height)
        {
            string rootPath = Server.MapPath("~");
            string path = Path.GetFullPath(Path.Combine(rootPath, FisiovidaSystem.Lib.Util.ConfiguracaoAppUtil.Get(FisiovidaSystem.Lib.Enumerator.enumConfiguracaoLib.MidiasUrlCrop) + fileName));

            byte[] imageBytes = System.IO.File.ReadAllBytes(path);
            byte[] croppedImage = MidiaHelper.CropImage(imageBytes, x, y, width, height);

            System.IO.File.WriteAllBytes(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, fileName), croppedImage);
        }
        public ActionResult Salvar(Configuracao configuracao, HttpPostedFileBase file1)
        {
            try
            {
                ConfiguracaoService service = new ConfiguracaoService();
                MidiasService mService = new MidiasService();

                if (file1 != null)
                {
                    RetornoModel<string> retorno = mService.Upload(file1);
                    if (retorno.Sucesso)
                    {
                        if (!string.IsNullOrEmpty(configuracao.BannerPropaganda))
                        {
                            System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, configuracao.BannerPropaganda));
                        }
                        configuracao.BannerPropaganda = retorno.Retorno;
                    }
                    Crop(configuracao.BannerPropaganda, configuracao.x, configuracao.y, configuracao.w, configuracao.h);
                }

                service.Salvar(configuracao);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Configuracao", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
            }
            return RedirectToAction("Index", "Configuracao", new { msg = "Configurações salvas com sucesso!", erro = false });
        }

    }
}