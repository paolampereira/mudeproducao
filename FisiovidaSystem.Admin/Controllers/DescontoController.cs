﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [AdminAuthorize(true)]
    public class DescontoController : MasterController
    {
        private DescontoService service = new DescontoService();
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Editar(int id = 0)
        {
            Desconto entidade = new Desconto();

            if (id > 0)
            {   
                try
                {
                    entidade = service.CarregarDados(id);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Detalhe", "Aluno", new { id = id, msg = "Ocorreu um erro ao carregar os dados", erro = true });
                }

            }
            return View(entidade);
        }

        public ActionResult Salvar(Desconto desconto)
        {
            try
            {
                service.Salvar(desconto);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Desconto", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
            }

            return RedirectToAction("Index", "Desconto", new { msg = "Dados salvos com sucesso!", erro = false});
        }

        public ActionResult SetInativo(int id, bool inativo)
        {
            try
            {
                Desconto desconto = new Desconto() { ID = id, Inativo = inativo };
                service.AlterarInativo(desconto);
            }
            catch (Exception e)
            {
                return Json(new RetornoModel() { Sucesso = false, Mensagem = "Erro ao salvar os dados" });
            }

            return Json(new RetornoModel() { Sucesso = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Listar(jQueryDataTableParamModel param)
        {
            IList<Desconto> result = new List<Desconto>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;

                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;

                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                listaDados.Add(new string[] {
                    item.Descricao,
                    (item.Porcentagem * 100) + "%",
                    item.Inativo + "-" + item.ID,
                    item.ID.ToString(),
                    item.ID.ToString(),
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;
        }

    }

}