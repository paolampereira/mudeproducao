﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Admin.Util;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Enumerator;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Lib.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [AdminAuthorize(true)]
    public class TratamentoController : MasterController
    {
        private TratamentoService service = new TratamentoService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Excluir(int id)
        {
            try
            {
                bool result = service.Excluir(id);
                if (!result)
                {
                    return RedirectToAction("Index", "Tratamento", new { erro = true, msg = "O tratamento não pode ser excluída pois contém turmas ativas" });
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Tratamento", new { erro = true, msg = "Ocorreu um erro ao excluir o tratamento" });
            }
            return RedirectToAction("Index", "Tratamento", new { erro = false, msg = "Tratamento excluído com sucesso!" });
        }

        private void Crop(string fileName, int x, int y, int width, int height)
        {
            string rootPath = Server.MapPath("~");
            string path = Path.GetFullPath(Path.Combine(rootPath, FisiovidaSystem.Lib.Util.ConfiguracaoAppUtil.Get(FisiovidaSystem.Lib.Enumerator.enumConfiguracaoLib.MidiasUrlCrop) + fileName));

            byte[] imageBytes = System.IO.File.ReadAllBytes(path);
            byte[] croppedImage = MidiaHelper.CropImage(imageBytes, x, y, width, height);

            System.IO.File.WriteAllBytes(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, fileName), croppedImage);
        }
        [HttpPost]
        public ActionResult Salvar(Tratamento tratamento)
        {
            try
            {
                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    MidiasService mService = new MidiasService();
                    RetornoModel<string> retorno = mService.Upload(file);

                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                }
                if (arquivos.Count == 1)
                {
                    if (!string.IsNullOrEmpty(tratamento.FotoCapa))
                    {
                        System.IO.File.Delete(ConfiguracaoAppUtil.GetPath(enumConfiguracaoLib.MidiasDiretorio, tratamento.FotoCapa));
                    }
                    tratamento.FotoCapa = arquivos[0];
                    Crop(tratamento.FotoCapa, tratamento.x, tratamento.y, tratamento.w, tratamento.h);
                }
                service.Salvar(tratamento);
            }
            catch (Exception ex)
            {
                if (tratamento.ID > 0)
                {
                    return RedirectToAction("Index", "Tratamento", new { msg = "Erro ao salvar os dados!", erro = true });
                }
                return RedirectToAction("Index", "Tratamento", new { msg = "Erro ao salvar os dados!", erro = true });
            }
            /*
            if (tratamento.ID > 0)
            {
                return RedirectToAction("Detalhe", "Modalidade", new { msg = "Modalidade editada com sucesso!", erro = false, id = modalidade.ID });
            }*/
            return RedirectToAction("Index", "Tratamento", new { msg = "Tratamento salvo com sucesso!", erro = false });
        }

        public ActionResult UploadImagens(int tratamentoID)
        {
            bool algumErro = false;
            Tratamento tratamento;
            try
            {

                List<string> arquivos = new List<string>();
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    MidiasService mService = new MidiasService();
                    RetornoModel<string> retorno = mService.Upload(file);
                    if (retorno.Sucesso)
                    {
                        arquivos.Add(retorno.Retorno);
                    }
                    else
                    {
                        algumErro = true;
                    }
                }
                if (arquivos.Count > 0)
                {
                    tratamento = service.RegistrarImagens(arquivos, tratamentoID);
                }
                else
                {
                    return Json(new { sucesso = false }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                string a = ex.Message;
                return Json(new { sucesso = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { sucesso = true, msg = RenderRazorViewToString("_GaleriaPartial", tratamento), algumerro = algumErro }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Editar(int id = 0)
        {
            Tratamento entidade = new Tratamento();
            
            entidade.TagsIds = new List<int>();

            try
            {
                if (id > 0)
                {
                    entidade = service.Carregar(id);
                }
                IconeService iService = new IconeService();
                entidade.IconesSistema = iService.Listar();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Tratamento", new { erro = true, msg = "Ocorreu um erro ao carregar os dados" });
            }

            if (entidade.IconesSistema == null)
            {
                entidade.IconesSistema = new List<Icone>();
            }
            if (entidade.Icones == null)
            {
                entidade.Icones = new List<Icone>();
            }
            return View(entidade);
        }

        public ActionResult Listar(jQueryDataTableParamModel param)
        {
            IList<Tratamento> result = new List<Tratamento>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;


                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;

                //Consulta dados no banco
                result = service.Listar(skip, take, palavraChave);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();
            foreach (var item in result)
            {
                var descr = item.Descricao;

                if (item.Descricao.Length > 100)
                {
                    descr = item.Descricao.Substring(0, 100) + "..";
                }

                listaDados.Add(new string[] {
                    item.Nome,
                    descr,
                    item.Duracao.ToString() + " min",
                    item.ID.ToString(),
                    item.ID.ToString() 
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;

        }

    }
}