﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [AdminAuthorize(true)]
    public class TurmaController : MasterController
    {
        private TurmaService service = new TurmaService();
        // GET: Turma
        public ActionResult Index()
        {
            TurmaSearchModel model = new TurmaSearchModel();
            try
            {
                UsuarioService uservice = new UsuarioService();
                model.Professores = uservice.ListarIdsNomesProfessores();
                ModalidadeService mservice = new ModalidadeService();
                model.Modalidades = mservice.ListarIdsNomes();
            }
            catch (Exception e)
            {

            }
            return View(model);
        }

        public ActionResult Cadastrar()
        {
            TurmaModel model = new TurmaModel()
            {
                Horarios = new List<Horario>(),
                Modalidades = new List<Modalidade>(),
                Professores = new List<Usuario>(),
                Turma = new Turma()
            };

            try
            {
                UsuarioService uservice = new UsuarioService();
                ModalidadeService mservice = new ModalidadeService();
                HorarioService hservice = new HorarioService();
                TratamentoService tService = new TratamentoService();

                model.Professores = uservice.ListarIdsNomesProfessores();
                model.Modalidades = mservice.ListarIdsNomes();
                model.Horarios = hservice.ListarHorarios();
                model.Tratamentos = tService.ListarIdsNomes();
            }
            catch (Exception ex)
            {

            }

            return View(model);
        }

        public ActionResult RealizarCadastro(TurmaModel turma)
        {
            try
            {
                Turma t = new Turma()
                {
                    Inativo = false,
                    HorarioID = turma.HorarioID,
                    ModalidadeID = turma.ModalidadeID,
                    TratamentoID = turma.TratamentoID,
                    ProfessorID = turma.ProfessorID,

                };
                if (turma.TipoTurma)
                {
                    t.TratamentoID = -1;
                }
                else
                {
                    t.ModalidadeID = -1;
                }
                service.Salvar(t);
            }
            catch (Exception ex)
            {
                if (ex.Message == "23505: duplicate key value violates unique constraint \"turma_modalidadeid_professorid_horarioid_key\"" ||
                    ex.Message == "23505: duplicate key value violates unique constraint \"turma_tratamentoid_professorid_horarioid_key\"")
                {
                    return RedirectToAction("Index", "Turma", new { msg = "Turma já existente", erro = true });
                }
                return RedirectToAction("Index", "Turma", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
            }

            return RedirectToAction("Index", "Turma", new { msg = "Ok", erro = false });
        }

        public ActionResult Detalhe(int id)
        {
            Turma entidade = new Turma();
            try
            {
                entidade = service.Carregar(id);
            }
            catch (Exception ex)
            {

            }
            return View(entidade);
        }

        public ActionResult Excluir(int id)
        {
            try
            {
                bool result = service.Excluir(id);
                if (!result)
                {
                    return RedirectToAction("Index", "Turma", new { erro = true, msg = "A turma não pode ser excluída pois contém alunos ativas" });
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Turma", new { erro = true, msg = "Ocorreu um erro ao excluir a turma" });
            }
            return RedirectToAction("Index", "Turma", new { erro = false, msg = "Turma excluída com sucesso!" });
        
        }

        public ActionResult Listar(jQueryDataTableParamModel param, int professorid = 0, int diaSemana = -1)
        {
            IList<Turma> result = new List<Turma>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;


                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take, diaSemana, professorid);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {

            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();

            foreach (var item in result)
            {
                var nome = "";
                if (item.Modalidade != null)
                {
                    nome = item.Modalidade.Nome;
                }
                else
                {
                    nome = item.Tratamento.Nome;
                }
                listaDados.Add(new string[] {
             
                    nome,
                    item.Horario.NomeDiaSemana,
                    item.Horario.Hora.ToString("H:mm"),
                    item.Professor.Nome,
                    item.VagasDisponiveis.ToString(),
                    item.ID.ToString(),
                    item.ID.ToString() 
             
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;


        }

    }
}