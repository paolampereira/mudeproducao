﻿using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    [AdminAuthorize(true)]
    public class HorarioController : MasterController
    {
        private HorarioService service = new HorarioService();
        // GET: Horario
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Excluir(int id)
        {
            try
            {
                bool semTurmas = service.Excluir(id);
                if (!semTurmas)
                {
                    return RedirectToAction("Index", "Horario", new { erro = true, msg = "Horário não pode ser excluído. Existem turma(s) no mesmo." });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Horario", new { erro = true, msg = "Ocorreu um erro inesperado ao excluir o horário" });
            }
            return RedirectToAction("Index", "Horario", new { erro = false, msg = "Horário excluído com sucesso!" });
        }

        public ActionResult Editar(int id = 0)
        {
            Horario entidade = new Horario();

            if (id > 0)
            {
                try
                {
                    entidade = service.Carregar(id);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Index", "Horario", new { msg = "Ocorreu um erro ao carregar os dados", erro = true });
                }

            }
            return View(entidade);
        }
        public ActionResult Salvar(Horario horario)
        {
            try
            {
                horario.UsuarioID = SessionModel.Usuario.ID;
                service.Salvar(horario);
            }
            catch (Exception ex)
            {
                if (ex.Message == "23505: duplicate key value violates unique constraint \"horario_diasemana_hora_key\"")
                {
                    return RedirectToAction("Index", "Horario", new { msg = "Horário já existente", erro = true });
                }
                return RedirectToAction("Index", "Horario", new { msg = "Ocorreu um erro ao salvar os dados", erro = true });
            }
            return RedirectToAction("Index", "Horario", new { msg = "Horário salvo com sucesso", erro = false });
        }

        public ActionResult Listar(jQueryDataTableParamModel param)
        {
            IList<Horario> result = new List<Horario>();
            int totalReg = 0;
            try
            {
                string palavraChave = (param.sSearch != null) ? param.sSearch.ToString() : string.Empty;


                //Corrige calculo paginação
                int skip = param.iDisplayStart;
                int take = param.iDisplayLength;


                //Consulta dados no banco
                result = service.Listar(skip, take);

                //Retorna total de registros para monstar a paginação
                totalReg = service.TotalRegistros;
            }
            catch (Exception ex)
            {
                return RedirectToAction("InternalServer", "Erro", new { erro = true, msg = "Ocorreu um erro inesperado ao carregar os dados" });
            }
            //Formata saida dos dados para funcionar no DataGrid obrigatório colocar todos parametros que vão aparecer lá
            List<string[]> listaDados = new List<string[]>();

            foreach (var item in result)
            {
                string por = "";
                if (item.Usuario != null)
                {
                    por = " por " + item.Usuario.Nome;
                }

                listaDados.Add(new string[] {
             
                    item.NomeDiaSemana,
                    item.Hora.ToString("H:mm"),
                    item.DataModificacao.ToString("dd/MM/yyyy") + por,
                    item.ID.ToString() 
             
                });
            }

            //Monta Json de Retorno
            JsonResult retorno = Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = totalReg,
                iTotalDisplayRecords = totalReg,
                aaData = listaDados
            },
            JsonRequestBehavior.AllowGet);

            return retorno;


        }
    }
}