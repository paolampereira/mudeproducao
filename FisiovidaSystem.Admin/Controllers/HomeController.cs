﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Admin.Controllers
{
    public class HomeController : MasterController
    {
        // GET: Home
        [Authorize]
        public ActionResult Index()
        {

            return RedirectToAction("MinhasTurmas", "Usuario");
        }
    }
}