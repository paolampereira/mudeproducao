﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Admin.Models
{
    public class AlunoModel
    {
        public Aluno Aluno { get; set; }
        public List<Modalidade> Modalidades { get; set; }
        public int Aba { get; set; }
        public List<Desconto> Descontos { get; set; }
    }
}