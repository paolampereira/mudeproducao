﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Admin.Models
{
    public class ProfessoresTurmaModel
    {
        public List<Usuario> Professores { get; set; }
    }
}