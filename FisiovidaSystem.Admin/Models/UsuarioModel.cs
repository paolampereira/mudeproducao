﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FisiovidaSystem.Admin.Models
{
    public class UsuarioModel
    {
        public string Login { get; set; }
        public string Senha { get; set; }
        public bool Lembrarme { get; set; }
    }
}