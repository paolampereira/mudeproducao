﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Admin.Models
{
    public class DesmarcarModel
    {
        public int AlunoID { get; set; }
        public int TurmaID { get; set; }
        public int PlanoID { get; set; }
        public string RestanteSemana { get; set; }
        public string StringCancelamentos { get; set; }
        public List<InscricaoTemp> Cancelamentos { get; set; }
    }
}