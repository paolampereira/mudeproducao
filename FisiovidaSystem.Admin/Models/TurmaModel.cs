﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Admin.Models
{
    public class TurmaModel
    {
        public Turma Turma { get; set; }
        public List<Usuario> Professores { get; set; }
        public List<Horario> Horarios { get; set; }
        public List<Modalidade> Modalidades { get; set; }
        public List<Tratamento> Tratamentos { get; set; }
        public int TratamentoID { get; set; }
        public int ModalidadeID { get; set; }
        public bool TipoTurma { get; set; }
        public int HorarioID { get; set; }
        public int ProfessorID { get; set; }
    }
}