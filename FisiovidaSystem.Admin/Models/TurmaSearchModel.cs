﻿using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Enumerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FisiovidaSystem.Admin.Models
{
    public class TurmaSearchModel
    {
        public List<Usuario> Professores { get; set; }
        public List<Modalidade> Modalidades { get; set; }
    }
}