﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FisiovidaSystem.Admin.Models
{
    public class PacoteModalidadeModel
    {
        public int ID { get; set; }
        public int ModalidadeID { get; set; }
        public string ModalidadeNome { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }
        public int QtdDias { get; set; }
    }
}