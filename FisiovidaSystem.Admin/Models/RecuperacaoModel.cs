﻿using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FisiovidaSystem.Admin.Models
{
    public class RecuperacaoModel
    {
        public int AlunoID { get; set; }
        public int PlanoID { get; set; }
        public string RestanteSemana { get; set; }
        public string StringProibidos { get; set; }
        public List<InscricaoTemp> Recuperacoes { get; set; }

    }
}