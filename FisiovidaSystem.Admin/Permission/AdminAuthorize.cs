﻿using FisiovidaSystem.Admin.Controllers;
using FisiovidaSystem.Admin.Models;
using FisiovidaSystem.Lib.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;


[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public class AdminAuthorize : AuthorizeAttribute
{
    private bool _master = false;
    private SessionModel sessionModel = null;

    public AdminAuthorize() { }

    public AdminAuthorize(bool master)
    {
        _master = master;
    }

    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {
        sessionModel = MasterController.CarregaSessao(httpContext.Session, httpContext.User);
        return UsuarioService.TemPermissao(sessionModel.Usuario, _master);
    }

    protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
    {
        base.HandleUnauthorizedRequest(filterContext);

        if (sessionModel.Usuario == null)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Conta", action = "Index" }));
        }
        else
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Index" }));
        }

    }
}
