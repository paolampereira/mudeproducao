$(".page .items").autobrowse(
    {
        url: function (offset)
        {
            return "http://api.flickr.com/services/feeds/photos_public.gne?tags=cat&tagmode=any&format=json&jsoncallback=?";
        },
        template: function (response)
        {
            var markup='';
            for (var i=0; i<response.items.length; i++)
            {
                markup+='<a href="'+response.items[i].link+'"><img src="'+response.items[i].media.m+'" /></a>'
            };
            return markup;
        },
        itemsReturned: function (response) { return response.items.length; },
        offset: 0,
        max: 100,
        loader: '<div class="loader"></div>',
        useCache: true,
        expiration: 1
    }
);
    