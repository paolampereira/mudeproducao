$(function () {

    function initMap() {

        var location = new google.maps.LatLng(-32.1848295,-52.1585323);

        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: location,
            zoom: 18,
            panControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var markerImage = '../../imagem/marker-map.png';

        var marker = new google.maps.Marker({
            position: location,
            map: map,
            icon: markerImage
        });

     

    }

    google.maps.event.addDomListener(window, 'load', initMap);
});