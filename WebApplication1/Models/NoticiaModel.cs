﻿using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FisiovidaSystem.Models
{
    public class NoticiaModel
    {
        public Novidade Noticia { get; set; }
        public Configuracao Configuracao { get; set; }
    }
}