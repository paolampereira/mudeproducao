﻿using FisiovidaSystem.Lib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FisiovidaSystem.Models
{
    public class BlogModel
    {
        public List<Novidade> Noticias { get; set; }
        public Configuracao Configuracao { get; set; }
        public int TotalNoticias { get; set; }
    }
}