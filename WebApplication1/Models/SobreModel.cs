﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Models
{
    public class SobreModel
    {
        public List<Tag> Diferenciais { get; set; }
        public Configuracao Configuracao { get; set; }
        public List<Usuario> Professores { get; set; }
        public int TotalProfessores { get; set; }
        public int TotalModalidades { get; set; }
        public int TotalAlunos { get; set; }
    }
}