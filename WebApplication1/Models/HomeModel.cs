﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Models
{
    public class HomeModel
    {
        public List<Novidade> Destaques { get; set; }
        public Configuracao Configuracao { get; set; }
    }
}