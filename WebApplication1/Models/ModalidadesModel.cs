﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Entity;

namespace FisiovidaSystem.Models
{
    public class ModalidadesModel
    {
        public List<ModalidadeTratamentoModel> Modalidades { get; set; }
        public Configuracao Configuracao { get; set; }
    }
}