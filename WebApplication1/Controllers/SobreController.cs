﻿using FisiovidaSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FisiovidaSystem.Lib.Service;

namespace FisiovidaSystem.Controllers
{
    public class SobreController : MasterController
    {
        // GET: Sobre
        public ActionResult Index()
        {
            SobreModel model = new SobreModel();
            try
            {
                TagService service = new TagService();
                model.Diferenciais = service.ListarDiferenciais();

                ConfiguracaoService configService = new ConfiguracaoService();
                model.Configuracao = configService.CarregarVariaveisSite();

                UsuarioService uService = new UsuarioService();
                model.Professores = uService.ListarProfessores();
                model.TotalProfessores = model.Professores.Count;

                ModalidadeService mService = new ModalidadeService();
                model.TotalModalidades = mService.TotalModalidadesTratamentos();
            }
            catch (Exception ex)
            {

            }

            return View(model);
        }
    }
}