﻿using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Controllers
{
    public class ContatoController : MasterController
    {
        // GET: Contato
        public ActionResult Index()
        {
            ContatoModel model = new ContatoModel();
            try
            {
                ConfiguracaoService configService = new ConfiguracaoService();
                model.Configuracao = configService.CarregarVariaveisContato();
            }
            catch (Exception ex)
            {
                return RedirectToAction("InternalServer", "Erro");
            }
            return View(model);
        }
    }
}