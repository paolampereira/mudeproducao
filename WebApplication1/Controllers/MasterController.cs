﻿using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Controllers
{
    public class MasterController : Controller
    {
        protected SessionModel SessionModel = null;
        public static Configuracao Configuracao = null;

        public static DateTime VariaveisUltimoCarregamento = DateTime.Now;
        public static bool VariaveisCarregadas = false;
        public static int VariaveisCarregamentoTTL = 20;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CarregaVariaveis();
            VerificaMsgRetorno();
            base.OnActionExecuting(filterContext);
        }

        protected void CarregaVariaveis()
        {
            SessionModel = CarregaSessao(Session, User);
            if ((!VariaveisCarregadas) || ((DateTime.Now - VariaveisUltimoCarregamento).Minutes > VariaveisCarregamentoTTL))
            {
                ConfiguracaoService config = new ConfiguracaoService();
                Configuracao = config.Carregar();

                VariaveisUltimoCarregamento = DateTime.Now;
                VariaveisCarregadas = true;

                if (SessionModel.Usuario == null)
                {
                    //HttpContext.GetOwinContext().Authentication.SignOut();
                }
            }
        }

        public static SessionModel CarregaSessao(HttpSessionStateBase Session, IPrincipal User)
        {

            SessionModel SessionModel = Session.GetModel<SessionModel>();

            if (User.Identity.IsAuthenticated)
            {
                if (SessionModel.Usuario == null)
                {
                    UsuarioService usuario = new UsuarioService();
                    //SessionModel.Usuario = usuario.Carregar(Convert.ToInt32(IdentityExtensions.GetUserId(User.Identity)));
                    Session.SaveModel(SessionModel);
                }
            }
            else
            {
                if (SessionModel.Usuario != null)
                {
                    SessionModel.Usuario = null;
                    Session.SaveModel(SessionModel);
                }
            }
            return SessionModel;
        }

        protected void SalvarSessao()
        {
            Session.SaveModel(this.SessionModel);
        }

        protected void VerificaMsgRetorno()
        {
            ViewBag.msg = "";
            ViewBag.erro = false;

            if (Request.QueryString["msg"] != null)
            {
                ViewBag.msg = Request.QueryString["msg"].ToString();
                bool erro = false;
                if (bool.TryParse(Request["erro"], out erro))
                {
                    ViewBag.erro = erro;
                }

            }
        }
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}