﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Controllers
{
    public class ErroController : MasterController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult NotFound()
        {
            Response.StatusCode = 404;  //you may want to set this to 200
            return View();
        }
        public ViewResult InternalServer()
        {
            Response.StatusCode = 500;  //you may want to set this to 200
            return View();
        }
    }
}