﻿using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Model;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Controllers
{
    public class ModalidadeController : MasterController
    {
        // GET: Modalidade
        public ActionResult Index()
        {
            ModalidadesModel model = new ModalidadesModel();
            try
            {
                ModalidadeService service = new ModalidadeService();
                model.Modalidades = service.ListarModalidadesSite();

                ConfiguracaoService configService = new ConfiguracaoService();
                model.Configuracao = configService.CarregarVariaveisSite();
            }
            catch (Exception e)
            {

            }
            return View(model);
        }

        public ActionResult Detalhe(string modelID)
        {
            ModalidadeTratamentoModel model = new ModalidadeTratamentoModel();
            try
            {
                ConfiguracaoService configService = new ConfiguracaoService();
                model.Configuracao = configService.CarregarVariaveisSite();

                int tipo = Int32.Parse(modelID.Substring(0,1));
                int id = Int32.Parse(modelID.Substring(1));
                UsuarioService uService = new UsuarioService();
                if (tipo == 1) // modalidades
                {
                    ModalidadeService service = new ModalidadeService();
                    Modalidade t = service.Carregar(id);
                    model.ID = t.ID;
                    model.Nome = t.Nome;
                    model.VagasPorTurma = t.VagasPorTurma;
                    model.Duracao = t.Duracao;
                    model.Galeria = t.Galeria;
                    model.Descricao = t.Descricao;
                    model.Icones = t.Icones;
                    model.Professores = uService.ListaProfessoresModalidade(t.ID, 0);

                }
                else // tratamentos
                {
                    TratamentoService tService = new TratamentoService();
                    Tratamento t = tService.Carregar(id);
                    model.ID = t.ID;
                    model.Nome = t.Nome;
                    model.VagasPorTurma = 1;
                    model.Duracao = t.Duracao;
                    model.Galeria = t.Galeria;
                    model.Descricao = t.Descricao;
                    model.Icones = t.Icones;
                    model.Professores = uService.ListaProfessoresModalidade(0, t.ID);
                }

            }
            catch (Exception e)
            {

            }
            return View(model);
        }
    }
}