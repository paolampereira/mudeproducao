﻿using FisiovidaSystem.Lib.ADO;
using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Controllers
{
    public class HomeController : MasterController
    {
        public ActionResult Index()
        {
            HomeModel model = new HomeModel();
            model.Destaques = new List<Novidade>();
            model.Configuracao = new Configuracao();
            /*try
            {*/
                NovidadeService service = new NovidadeService();
                model.Destaques = service.ListarDestaques();

                ConfiguracaoService configService = new ConfiguracaoService();
                model.Configuracao = configService.CarregarVariaveisSite();
            /*}
            catch (Exception ex)
            {
                return RedirectToAction("InternalServer", "Erro");
            }*/
            return View(model);
        }
    }
}