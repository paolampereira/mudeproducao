﻿using FisiovidaSystem.Lib.Entity;
using FisiovidaSystem.Lib.Service;
using FisiovidaSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FisiovidaSystem.Controllers
{
    public class BlogController : MasterController
    {
        // GET: Blog
        public ActionResult Index()
        {
            BlogModel model = new BlogModel();
            try
            {
                ConfiguracaoService configService = new ConfiguracaoService();
                model.Configuracao = configService.CarregarVariaveisContato();

                NovidadeService nService = new NovidadeService();
                model.Noticias = nService.Listar();
                model.TotalNoticias = nService.TotalRegistros;
            }
            catch (Exception e)
            {
                return RedirectToAction("InternalServer", "Erro");
            }
            return View(model);
        }
        public ActionResult Detalhe(int id)
        {
            NoticiaModel model = new NoticiaModel();
            try
            {
                ConfiguracaoService configService = new ConfiguracaoService();
                model.Configuracao = configService.CarregarVariaveisContato();


                NovidadeService nService = new NovidadeService();
                model.Noticia = nService.Carregar(id);
            }
            catch (Exception e)
            {
                return RedirectToAction("InternalServer", "Erro");
            }
            return View(model);
        }
        public ActionResult CarregaPagina(int pagina)
        {
            BlogModel model = new BlogModel();
            try
            {
                NovidadeService nService = new NovidadeService();
                model.Noticias = nService.Listar(pagina);
                model.TotalNoticias = nService.TotalRegistros;
            }
            catch (Exception e)
            {
                return RedirectToAction("InternalServer", "Erro");
            }
            return Json(new
            {
                sucesso = true,
                total = model.TotalNoticias,
                msg = RenderRazorViewToString("_NoticiasPartial", model)
            }, JsonRequestBehavior.AllowGet);
        }
    }
}